#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "blocks.h"

#pragma OPENCL EXTENSION cl_khr_fp64 : enable

typedef float value;

// Dimensions
/*
#define Nx 8
#define Ny 6
#define Nz 10
#define Nx 256
#define Ny 256
#define Nz 202
#define Nx 10
#define Ny 10
#define Nz 6
*/

#define Nx 256
#define Ny 256
#define Nz 202

/*

#define Nx 1024
#define Ny 512
#define Nz 256
*/

#define VOLUME Nx*Ny*Nz
#define AREA Nx*Ny

// Define Thread block size
/*#define Bx 64
#define By 4
#define Bz 1
*/
// Define Source and Read
/*
#define Sx 1
#define Sy 1
#define Sz 1
#define Rx 2
#define Ry 2
#define Rz 2
*/

#define Sx 120
#define Sy 120
#define Sz 60
#define Rx 50
#define Ry 50
#define Rz 50


#define MAX_STR_LEN 1024

// ------------------------------------------
// Simulation parameters					      

#define numberSamples 4410
#define iter 1
#define dim         3
#define dur         20

#define nano        1.e-09

#define pi 3.1415926535897932384626433832795



#endif
