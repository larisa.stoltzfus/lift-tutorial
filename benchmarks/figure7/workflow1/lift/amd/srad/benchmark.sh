#/bin/bash

DIR=$(pwd)
FILENAME=$(basename "${DIR}")

if [ "$#" -ne 5 ]; then
				echo "Illegal number of parameters (enter platform, device, iterations, rows and columns)"
                                echo "(IE. ./benchmark.sh 0 0 1 502 458)"
				exit -1
fi

N=$3

RAWDATA="${FILENAME}.raw"
OUT1="${FILENAME}1.out"
OUT2="${FILENAME}2.out"

if [ -e $RAWDATA ]
then
    rm $RAWDATA
fi

if [ -e $OUT1 ]
then
    rm $OUT1
fi

if [ -e $OUT2 ]
then
    rm $OUT2
fi

for i in $(seq 1 $N)
do
    ./srad 1 0.5 $4 $5 $1 $2 >> srad.raw
done

cat $RAWDATA | grep SRAD | awk '{print $2}' > $OUT1
cat $RAWDATA | grep SRAD | awk '{print $4}' > $OUT2
cat $OUT1
cat $OUT2
