
// High-level hash: e5cc2647cb7fde27cecfc7d35fa525d2ad04ac8bd5131f996954afe6b2d2393c
// Low-level hash: a425bf0491d0d93ae80f895079e89b9bec6f340c93ff0965d327eaed42833c3a

#atf::var<int> v_M_101 = 502
#atf::var<int> v_N_102 = 458

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(900)"

#atf::tp name "v_TP_143" \
 type "int" \
 range "atf::interval<int>(1,v_M_101)" \
 constraint "atf::divides(v_M_101)"
#atf::tp name "v_TP_142" \
 type "int" \
 range "atf::interval<int>(1,v_N_102)" \
 constraint "atf::divides(v_N_102)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,v_N_102)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,v_M_101)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1) && [&](auto & LOCAL_SIZE_1){ return LOCAL_SIZE_0 * LOCAL_SIZE_1 >= 32; }"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_101*v_N_102))"
#atf::ocl::input "atf::buffer<float>((v_M_101*v_N_102))"
#atf::ocl::input "atf::buffer<float>((v_M_101*v_N_102))"
#atf::ocl::input "atf::buffer<float>((v_M_101*v_N_102))"
#atf::ocl::input "atf::buffer<float>((v_M_101*v_N_102))"
#atf::ocl::input "atf::buffer<float>((v_M_101*v_N_102))"
#atf::ocl::input "atf::buffer<float>((v_M_101*v_N_102))"
#atf::ocl::input "atf::scalar<int>(v_M_101)"
#atf::ocl::input "atf::scalar<int>(v_N_102)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float calculateImageUpdate(float img, float div){
  { return img + 0.125 * div; }
}
float calculateDiv(float dN, float dS, float dW, float dE, float orgDn, float orgDs, float orgDw, float orgDe){
  { return  (dN*orgDn + dS*orgDs + dW*orgDw + dE*orgDe) ; }
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__175, const global float* restrict v__176, const global float* restrict v__177, const global float* restrict v__178, const global float* restrict v__179, const global float* restrict v__180, global float* v__189, int v_M_101, int v_N_102){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__186_0;
  
  float v__188_0;
  
  for (int v_gl_id_171 = get_global_id(1); v_gl_id_171 < (v_N_102 / v_TP_142); v_gl_id_171 = (v_gl_id_171 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_172 = get_global_id(0); v_gl_id_172 < (v_M_101 / v_TP_143); v_gl_id_172 = (v_gl_id_172 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_173 = 0; v_i_173 < v_TP_142; v_i_173 = (1 + v_i_173)) {
        /* map_seq */
        for (int v_i_174 = 0; v_i_174 < v_TP_143; v_i_174 = (1 + v_i_174)) {
          v__186_0 = calculateDiv(v__176[((v_M_101 * ( ((-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) >= 0) ? ( ((-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) < v_N_102) ? (-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) : (-1 + v_N_102) ) : 0 )) + ( ((((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) >= 0) ? ( ((((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) < v_M_101) ? (((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) : (-1 + v_M_101) ) : 0 ))], v__176[((v_M_101 * ( ((-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((2 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) >= 0) ? ( ((-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((2 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) < v_N_102) ? (-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((2 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) : (-1 + v_N_102) ) : 0 )) + ( ((((v_i_174 + (2 * v_TP_143)) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((2 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) >= 0) ? ( ((((v_i_174 + (2 * v_TP_143)) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((2 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) < v_M_101) ? (((v_i_174 + (2 * v_TP_143)) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((2 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) : (-1 + v_M_101) ) : 0 ))], v__176[((v_M_101 * ( ((-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) >= 0) ? ( ((-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) < v_N_102) ? (-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) : (-1 + v_N_102) ) : 0 )) + ( ((((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) >= 0) ? ( ((((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) < v_M_101) ? (((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) : (-1 + v_M_101) ) : 0 ))], v__176[((v_M_101 * ( ((-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) >= 0) ? ( ((-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) < v_N_102) ? (-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) : (-1 + v_N_102) ) : 0 )) + ( ((1 + ((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) >= 0) ? ( ((1 + ((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) < v_M_101) ? (1 + ((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) : (-1 + v_M_101) ) : 0 ))], v__177[(v_i_174 + (v_M_101 * v_TP_142 * v_gl_id_171) + (v_M_101 * (v_gl_id_172 / (v_M_101 / v_TP_143))) + (v_M_101 * (((v_M_101 / v_TP_143) * ((v_i_173 + (v_TP_142 * v_gl_id_172)) % v_TP_142)) / (v_M_101 / v_TP_143))) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((v_i_173 + (v_TP_142 * v_gl_id_172)) % v_TP_142))) % (v_M_101 / v_TP_143))))], v__178[(v_i_174 + (v_M_101 * v_TP_142 * v_gl_id_171) + (v_M_101 * (v_gl_id_172 / (v_M_101 / v_TP_143))) + (v_M_101 * (((v_M_101 / v_TP_143) * ((v_i_173 + (v_TP_142 * v_gl_id_172)) % v_TP_142)) / (v_M_101 / v_TP_143))) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((v_i_173 + (v_TP_142 * v_gl_id_172)) % v_TP_142))) % (v_M_101 / v_TP_143))))], v__180[(v_i_174 + (v_M_101 * v_TP_142 * v_gl_id_171) + (v_M_101 * (v_gl_id_172 / (v_M_101 / v_TP_143))) + (v_M_101 * (((v_M_101 / v_TP_143) * ((v_i_173 + (v_TP_142 * v_gl_id_172)) % v_TP_142)) / (v_M_101 / v_TP_143))) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((v_i_173 + (v_TP_142 * v_gl_id_172)) % v_TP_142))) % (v_M_101 / v_TP_143))))], v__179[(v_i_174 + (v_M_101 * v_TP_142 * v_gl_id_171) + (v_M_101 * (v_gl_id_172 / (v_M_101 / v_TP_143))) + (v_M_101 * (((v_M_101 / v_TP_143) * ((v_i_173 + (v_TP_142 * v_gl_id_172)) % v_TP_142)) / (v_M_101 / v_TP_143))) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((v_i_173 + (v_TP_142 * v_gl_id_172)) % v_TP_142))) % (v_M_101 / v_TP_143))))]); 
          v__188_0 = calculateImageUpdate(v__175[((v_M_101 * ( ((-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) >= 0) ? ( ((-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) < v_N_102) ? (-1 + (v_gl_id_172 / (v_M_101 / v_TP_143)) + (((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142))) / (v_M_101 / v_TP_143)) + (v_TP_142 * v_gl_id_171)) : (-1 + v_N_102) ) : 0 )) + ( ((((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) >= 0) ? ( ((((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) < v_M_101) ? (((v_TP_143 + v_i_174) % v_TP_143) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((1 + v_i_173) % (2 + v_TP_142)))) % (v_M_101 / v_TP_143)))) : (-1 + v_M_101) ) : 0 ))], v__186_0); 
          v__189[(v_i_174 + (v_M_101 * v_TP_142 * v_gl_id_171) + (v_M_101 * (v_gl_id_172 / (v_M_101 / v_TP_143))) + (v_M_101 * (((v_M_101 / v_TP_143) * ((v_i_173 + (v_TP_142 * v_gl_id_172)) % v_TP_142)) / (v_M_101 / v_TP_143))) + (v_TP_143 * ((v_gl_id_172 + ((v_M_101 / v_TP_143) * ((v_i_173 + (v_TP_142 * v_gl_id_172)) % v_TP_142))) % (v_M_101 / v_TP_143))))] = id(v__188_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


