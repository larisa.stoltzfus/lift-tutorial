__kernel void hotspotOpt1(__global float *p, __global float* tIn, __global float *tOut, float sdc,
                            int nx, int ny, int nz,
                            float ce, float cw, 
                            float cn, float cs,
                            float ct, float cb, 
                            float cc) 
{
  float amb_temp = 80.0;

  int i = get_global_id(0);
  int j = get_global_id(1);
  int k = get_global_id(2);
  int xy = nx * ny;
  int c = i + j * nx + k * xy;

  int W = (i == 0)        ? c : c - 1;
  int E = (i == nx-1)     ? c : c + 1;
  int N = (j == 0)        ? c : c - nx;
  int S = (j == ny-1)     ? c : c + nx;
  int B = (k == 0)        ? c : c - xy
  int T = (k == nz-1)     ? c : c + xy

  tOut[c] =  tIn[c];// + cw * tIn[W] + ce * tIn[E] + cs * tIn[S]
    + cn * tIn[N] + cb * tIn[B] + ct * tIn[T] + sdc * p[c] + ct * amb_temp;
 // tOut[c] = cc * tIn[c] + cw * tIn[W] + ce * tIn[E] + cs * tIn[S]
 //   + cn * tIn[N] + cb * tIn[B] + ct * tIn[T] + sdc * p[c] + ct * amb_temp;
  return;
}


