//========================================================================================================================================================================================================200
//	DEFINE / INCLUDE
//========================================================================================================================================================================================================200

// *************************
// REESE SRAD KERNEL 1 
// *************************

float subtract(float l, float r){
  { return l - r; }
}
float calculateScoeff(float dN, float dS, float dE, float dW, float jC, float q0sqr){
  { float g2 = (dN*dN + dS*dS + dW*dW + dE*dE) / (jC * jC);float l = (dN + dS + dW + dE ) / jC; float num = (0.5*g2) - ((1.0/16.0)*(l*l)); float  den = 1 + (0.25*l);float qsqr = num/(den*den); den = (qsqr-q0sqr) / (q0sqr * (1+q0sqr));float coeff = 1.0 / (1.0+den);  if(coeff > 1) { return 1.0f; } else if(coeff < 0 ) { return 0.0f; } else { return coeff;}  }
}
float id(float x){
  { return x; }
}
kernel void srad_kernel_lift1(const global float* restrict v__15, float v__16, global float* v__28, int v_M_3, int v_N_4){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__19;
  float v__21;
  float v__23;
  float v__25;
  float v__27;
  for (int v_gl_id_13 = get_global_id(1);v_gl_id_13<v_N_4;v_gl_id_13 = (v_gl_id_13 + get_global_size(1))){
    for (int v_gl_id_14 = get_global_id(0);v_gl_id_14<v_M_3;v_gl_id_14 = (v_gl_id_14 + get_global_size(0))){
      v__19 = subtract(v__15[((v_M_3 * ( ((-1 + v_gl_id_13) >= 0) ? ( ((-1 + v_gl_id_13) < v_N_4) ? (-1 + v_gl_id_13) : ((-1 * v_gl_id_13) + (2 * v_N_4)) ) : (-1 * v_gl_id_13) )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_3) ? v_gl_id_14 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_14)) ) : (-1 + (-1 * v_gl_id_14)) ))], v__15[((v_M_3 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_4) ? v_gl_id_13 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_3) ? v_gl_id_14 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_14)) ) : (-1 + (-1 * v_gl_id_14)) ))]);
      v__21 = subtract(v__15[((v_M_3 * ( ((1 + v_gl_id_13) >= 0) ? ( ((1 + v_gl_id_13) < v_N_4) ? (1 + v_gl_id_13) : (-2 + (2 * v_N_4) + (-1 * v_gl_id_13)) ) : (-2 + (-1 * v_gl_id_13)) )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_3) ? v_gl_id_14 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_14)) ) : (-1 + (-1 * v_gl_id_14)) ))], v__15[((v_M_3 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_4) ? v_gl_id_13 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_3) ? v_gl_id_14 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_14)) ) : (-1 + (-1 * v_gl_id_14)) ))]);
      v__23 = subtract(v__15[((v_M_3 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_4) ? v_gl_id_13 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) )) + ( ((1 + v_gl_id_14) >= 0) ? ( ((1 + v_gl_id_14) < v_M_3) ? (1 + v_gl_id_14) : (-2 + (2 * v_M_3) + (-1 * v_gl_id_14)) ) : (-2 + (-1 * v_gl_id_14)) ))], v__15[((v_M_3 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_4) ? v_gl_id_13 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_3) ? v_gl_id_14 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_14)) ) : (-1 + (-1 * v_gl_id_14)) ))]);
      v__25 = subtract(v__15[((v_M_3 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_4) ? v_gl_id_13 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) )) + ( ((-1 + v_gl_id_14) >= 0) ? ( ((-1 + v_gl_id_14) < v_M_3) ? (-1 + v_gl_id_14) : ((-1 * v_gl_id_14) + (2 * v_M_3)) ) : (-1 * v_gl_id_14) ))], v__15[((v_M_3 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_4) ? v_gl_id_13 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_3) ? v_gl_id_14 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_14)) ) : (-1 + (-1 * v_gl_id_14)) ))]);
      v__27 = calculateScoeff(v__19, v__21, v__23, v__25, v__15[((v_M_3 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_4) ? v_gl_id_13 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_3) ? v_gl_id_14 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_14)) ) : (-1 + (-1 * v_gl_id_14)) ))], v__16);
      v__28[(v_gl_id_14 + (v_M_3 * v_gl_id_13))] = id(v__27);
    }
  }
}}

// ******************


// *************************


float calculateImageUpdate(float img, float div){
  { return img + 0.125 * div; }
}
float calculateDiv(float dN, float dS, float dW, float dE, float orgDn, float orgDs, float orgDw, float orgDe){
  { return  (dN*orgDn + dS*orgDs + dW*orgDw + dE*orgDe) ; }
}

// *************************
// REESE SRAD KERNEL INPT  2 
// *************************
kernel void srad_kernel_lift2inp(const global float* restrict v__23, const global float* restrict v__24, const global float* restrict v__25, const global float* restrict v__26, const global float* restrict v__27, const global float* restrict v__28, global float* v__36, int v_M_3, int v_N_4){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__33;
  float v__35;
  if(get_global_id(0) < v_M_3 && get_global_id(1) < v_N_4)
  {
  for (int v_gl_id_21 = get_global_id(1);v_gl_id_21<v_N_4;v_gl_id_21 = (v_gl_id_21 + get_global_size(1))){
    for (int v_gl_id_22 = get_global_id(0);v_gl_id_22<v_M_3;v_gl_id_22 = (v_gl_id_22 + get_global_size(0))){
      v__33 = calculateDiv(v__24[((v_M_3 * ( (v_gl_id_21 >= 0) ? ( (v_gl_id_21 < v_N_4) ? v_gl_id_21 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_21)) ) : (-1 + (-1 * v_gl_id_21)) )) + ( (v_gl_id_22 >= 0) ? ( (v_gl_id_22 < v_M_3) ? v_gl_id_22 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_22)) ) : (-1 + (-1 * v_gl_id_22)) ))], v__24[((v_M_3 * ( ((1 + v_gl_id_21) >= 0) ? ( ((1 + v_gl_id_21) < v_N_4) ? (1 + v_gl_id_21) : (-2 + (2 * v_N_4) + (-1 * v_gl_id_21)) ) : (-2 + (-1 * v_gl_id_21)) )) + ( (v_gl_id_22 >= 0) ? ( (v_gl_id_22 < v_M_3) ? v_gl_id_22 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_22)) ) : (-1 + (-1 * v_gl_id_22)) ))], v__24[((v_M_3 * ( (v_gl_id_21 >= 0) ? ( (v_gl_id_21 < v_N_4) ? v_gl_id_21 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_21)) ) : (-1 + (-1 * v_gl_id_21)) )) + ( (v_gl_id_22 >= 0) ? ( (v_gl_id_22 < v_M_3) ? v_gl_id_22 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_22)) ) : (-1 + (-1 * v_gl_id_22)) ))], v__24[((v_M_3 * ( (v_gl_id_21 >= 0) ? ( (v_gl_id_21 < v_N_4) ? v_gl_id_21 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_21)) ) : (-1 + (-1 * v_gl_id_21)) )) + ( ((1 + v_gl_id_22) >= 0) ? ( ((1 + v_gl_id_22) < v_M_3) ? (1 + v_gl_id_22) : (-2 + (2 * v_M_3) + (-1 * v_gl_id_22)) ) : (-2 + (-1 * v_gl_id_22)) ))], v__25[(v_gl_id_22 + (v_M_3 * v_gl_id_21))], v__26[(v_gl_id_22 + (v_M_3 * v_gl_id_21))], v__28[(v_gl_id_22 + (v_M_3 * v_gl_id_21))], v__27[(v_gl_id_22 + (v_M_3 * v_gl_id_21))]);
      v__35 = calculateImageUpdate(v__23[((v_M_3 * ( (v_gl_id_21 >= 0) ? ( (v_gl_id_21 < v_N_4) ? v_gl_id_21 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_21)) ) : (-1 + (-1 * v_gl_id_21)) )) + ( (v_gl_id_22 >= 0) ? ( (v_gl_id_22 < v_M_3) ? v_gl_id_22 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_22)) ) : (-1 + (-1 * v_gl_id_22)) ))], v__33);
      v__36[(v_gl_id_22 + (v_M_3 * v_gl_id_21))] = id(v__35);
    }
  }}
}}

