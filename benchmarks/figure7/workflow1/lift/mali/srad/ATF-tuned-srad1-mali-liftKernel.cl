
// High-level hash: 8dbfc2361134047c70b7246f35bc516900cb12f5469167c7d973f5d2c40a9495
// Low-level hash: 11518e13099656748eaa85d308ab96d19a9ae4101c48d48b39e9732ad2684dff

#atf::var<int> v_M_0 = 502
#atf::var<int> v_N_1 = 458

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(450)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,v_N_1)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,v_M_0)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1) && [&](auto & LOCAL_SIZE_1){ return LOCAL_SIZE_0 * LOCAL_SIZE_1 >= 4; }"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::scalar<int>(v_M_0)"
#atf::ocl::input "atf::scalar<int>(v_N_1)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float subtract(float l, float r){
  { return l - r; }
}
float calculateScoeff(float dN, float dS, float dE, float dW, float jC, float q0sqr){
  { float g2 = (dN*dN + dS*dS + dW*dW + dE*dE) / (jC * jC);float l = (dN + dS + dW + dE ) / jC; float num = (0.5*g2) - ((1.0/16.0)*(l*l)); float  den = 1 + (0.25*l);float qsqr = num/(den*den); den = (qsqr-q0sqr) / (q0sqr * (1+q0sqr));float coeff = 1.0 / (1.0+den);  if(coeff > 1) { return 1.0f; } else if(coeff < 0 ) { return 0.0f; } else { return coeff;}  }
}
float idfloat(float x){
  { return x; }
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__35, global float* v__48, int v_M_0, int v_N_1){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__44; 
  /* Private Memory */
  float v__37; 
  float v__39; 
  float v__41; 
  float v__43; 
  float v__45; 
  float v__47; 
  for (int v_gl_id_33 = get_global_id(0); v_gl_id_33 < v_N_1; v_gl_id_33 = (v_gl_id_33 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_34 = get_global_id(1); v_gl_id_34 < v_M_0; v_gl_id_34 = (v_gl_id_34 + GLOBAL_SIZE_1)) {
      v__37 = subtract(v__35[((v_M_0 * ( ((-1 + v_gl_id_33) >= 0) ? ( ((-1 + v_gl_id_33) < v_N_1) ? (-1 + v_gl_id_33) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_34 >= 0) ? ( (v_gl_id_34 < v_M_0) ? v_gl_id_34 : (-1 + v_M_0) ) : 0 ))], v__35[((v_M_0 * ( (v_gl_id_33 >= 0) ? ( (v_gl_id_33 < v_N_1) ? v_gl_id_33 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_34 >= 0) ? ( (v_gl_id_34 < v_M_0) ? v_gl_id_34 : (-1 + v_M_0) ) : 0 ))]); 
      v__39 = subtract(v__35[((v_M_0 * ( ((1 + v_gl_id_33) >= 0) ? ( ((1 + v_gl_id_33) < v_N_1) ? (1 + v_gl_id_33) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_34 >= 0) ? ( (v_gl_id_34 < v_M_0) ? v_gl_id_34 : (-1 + v_M_0) ) : 0 ))], v__35[((v_M_0 * ( (v_gl_id_33 >= 0) ? ( (v_gl_id_33 < v_N_1) ? v_gl_id_33 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_34 >= 0) ? ( (v_gl_id_34 < v_M_0) ? v_gl_id_34 : (-1 + v_M_0) ) : 0 ))]); 
      v__41 = subtract(v__35[((v_M_0 * ( (v_gl_id_33 >= 0) ? ( (v_gl_id_33 < v_N_1) ? v_gl_id_33 : (-1 + v_N_1) ) : 0 )) + ( ((1 + v_gl_id_34) >= 0) ? ( ((1 + v_gl_id_34) < v_M_0) ? (1 + v_gl_id_34) : (-1 + v_M_0) ) : 0 ))], v__35[((v_M_0 * ( (v_gl_id_33 >= 0) ? ( (v_gl_id_33 < v_N_1) ? v_gl_id_33 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_34 >= 0) ? ( (v_gl_id_34 < v_M_0) ? v_gl_id_34 : (-1 + v_M_0) ) : 0 ))]); 
      v__43 = subtract(v__35[((v_M_0 * ( (v_gl_id_33 >= 0) ? ( (v_gl_id_33 < v_N_1) ? v_gl_id_33 : (-1 + v_N_1) ) : 0 )) + ( ((-1 + v_gl_id_34) >= 0) ? ( ((-1 + v_gl_id_34) < v_M_0) ? (-1 + v_gl_id_34) : (-1 + v_M_0) ) : 0 ))], v__35[((v_M_0 * ( (v_gl_id_33 >= 0) ? ( (v_gl_id_33 < v_N_1) ? v_gl_id_33 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_34 >= 0) ? ( (v_gl_id_34 < v_M_0) ? v_gl_id_34 : (-1 + v_M_0) ) : 0 ))]); 
      float v_tmp_173 = 0.05378722f; 
      v__44 = v_tmp_173; 
      v__45 = idfloat(v__44); 
      v__47 = calculateScoeff(v__37, v__39, v__41, v__43, v__35[((v_M_0 * ( (v_gl_id_33 >= 0) ? ( (v_gl_id_33 < v_N_1) ? v_gl_id_33 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_34 >= 0) ? ( (v_gl_id_34 < v_M_0) ? v_gl_id_34 : (-1 + v_M_0) ) : 0 ))], v__45); 
      v__48[(v_gl_id_34 + (v_M_0 * v_gl_id_33))] = id(v__47); 
    }
  }
}}


