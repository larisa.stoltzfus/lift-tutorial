
// High-level hash: 3d290fb326e109ac754ab0c09ad1e5fa9d989b653a8c6fc9cbf098cfce0a5e14
// Low-level hash: 60cb7c4a2a3034430de334ac491e5dc6e976f95c6cffd966619dd0664e66edbf

#atf::var<int> v_M_0 = 4098
#atf::var<int> v_N_1 = 4098

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(450)"

#atf::abort_condition "atf::cond::evaluations(1)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(4096,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(64,64)" constraint " own[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(64,64)" \
 constraint "atf::divides(GLOBAL_SIZE_1) && [&](auto & LOCAL_SIZE_1){ return LOCAL_SIZE_0 * LOCAL_SIZE_1 >= 4; }"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::buffer<float>(9)"
#atf::ocl::input "atf::buffer<float>((4+(-2*v_M_0)+(-2*v_N_1)+(v_M_0*v_N_1)))"
#atf::ocl::input "atf::scalar<int>(v_M_0)"
#atf::ocl::input "atf::scalar<int>(v_N_1)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct {
  float _0;
  float _1;
} Tuple2_float_float;
#endif

float idfloat(float x){
  { return x; }
}
float add(float x, float y){
  { return x+y; }
}
float mult(float l, float r){
  { return l * r; }
}
kernel void KERNEL(const global float* restrict v__47, const global float* restrict v__48, global float* v__69, int v_M_0, int v_N_1){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__49; 
  /* Private Memory */
  float v__57; 
  float v__64; 
  for (int v_gl_id_43 = get_global_id(0); v_gl_id_43 < (-2 + v_N_1); v_gl_id_43 = (v_gl_id_43 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_44 = get_global_id(1); v_gl_id_44 < (-2 + v_M_0); v_gl_id_44 = (v_gl_id_44 + GLOBAL_SIZE_1)) {
      float v_tmp_170 = 0.0f; 
      v__49 = v_tmp_170; 
      v__57 = idfloat(v__49); 
      /* reduce_seq */
      /* unroll */
      v__64 = mult(v__48[0], v__47[(v_gl_id_44 + (v_M_0 * v_gl_id_43))]); 
      v__57 = add(v__57, v__64); 
      v__64 = mult(v__48[1], v__47[(1 + v_gl_id_44 + (v_M_0 * v_gl_id_43))]); 
      v__57 = add(v__57, v__64); 
      v__64 = mult(v__48[2], v__47[(2 + v_gl_id_44 + (v_M_0 * v_gl_id_43))]); 
      v__57 = add(v__57, v__64); 
      v__64 = mult(v__48[3], v__47[(v_M_0 + ((-2 + v_M_0 + v_gl_id_44) % (-2 + v_M_0)) + (v_M_0 * v_gl_id_43))]); 
      v__57 = add(v__57, v__64); 
      v__64 = mult(v__48[4], v__47[(1 + v_M_0 + ((-2 + v_M_0 + v_gl_id_44) % (-2 + v_M_0)) + (v_M_0 * v_gl_id_43))]); 
      v__57 = add(v__57, v__64); 
      v__64 = mult(v__48[5], v__47[(2 + v_M_0 + ((-2 + v_M_0 + v_gl_id_44) % (-2 + v_M_0)) + (v_M_0 * v_gl_id_43))]); 
      v__57 = add(v__57, v__64); 
      v__64 = mult(v__48[6], v__47[(((-4 + v_gl_id_44 + (2 * v_M_0)) % (-2 + v_M_0)) + (v_M_0 * v_gl_id_43) + (v_M_0 * ((-4 + v_gl_id_44 + (2 * v_M_0)) / (-2 + v_M_0))))]); 
      v__57 = add(v__57, v__64); 
      v__64 = mult(v__48[7], v__47[(1 + ((-4 + v_gl_id_44 + (2 * v_M_0)) % (-2 + v_M_0)) + (v_M_0 * v_gl_id_43) + (v_M_0 * ((-4 + v_gl_id_44 + (2 * v_M_0)) / (-2 + v_M_0))))]); 
      v__57 = add(v__57, v__64); 
      v__64 = mult(v__48[8], v__47[(2 + ((-4 + v_gl_id_44 + (2 * v_M_0)) % (-2 + v_M_0)) + (v_M_0 * v_gl_id_43) + (v_M_0 * ((-4 + v_gl_id_44 + (2 * v_M_0)) / (-2 + v_M_0))))]); 
      v__57 = add(v__57, v__64); 
      /* end unroll */
      /* end reduce_seq */
      /* map_seq */
      /* unroll */
      v__69[(v_gl_id_44 + (-2 * v_gl_id_43) + (v_M_0 * v_gl_id_43))] = idfloat(v__57); 
      /* end unroll */
      /* end map_seq */
    }
  }
}}


