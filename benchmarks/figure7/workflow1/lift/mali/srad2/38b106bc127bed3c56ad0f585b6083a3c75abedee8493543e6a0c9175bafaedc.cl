
// High-level hash: 934c7ed8d67a0a625a910e2b166732715662ea25a0550eeaee6806f68d19fa52
// Low-level hash: 38b106bc127bed3c56ad0f585b6083a3c75abedee8493543e6a0c9175bafaedc

#atf::var<int> v_M_0 = 502
#atf::var<int> v_N_1 = 458

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(450)"
#atf::abort_condition "atf::cond::evaluations(1)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(256,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(64,64)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(64,64)" \
 constraint "atf::divides(GLOBAL_SIZE_1) && [&](auto & LOCAL_SIZE_1){ return LOCAL_SIZE_0 * LOCAL_SIZE_1 >= 4; }"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::scalar<int>(v_M_0)"
#atf::ocl::input "atf::scalar<int>(v_N_1)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float calculateImageUpdate(float img, float div){
  { return img + 0.125 * div; }
}
float calculateDiv(float dN, float dS, float dW, float dE, float orgDn, float orgDs, float orgDw, float orgDe){
  { return  (dN*orgDn + dS*orgDs + dW*orgDw + dE*orgDe) ; }
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__39, const global float* restrict v__42, const global float* restrict v__43, const global float* restrict v__44, const global float* restrict v__45, const global float* restrict v__47, global float* v__63, int v_M_0, int v_N_1){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__56; 
  float v__62; 
  for (int v_gl_id_37 = get_global_id(0); v_gl_id_37 < v_N_1; v_gl_id_37 = (v_gl_id_37 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_38 = get_global_id(1); v_gl_id_38 < v_M_0; v_gl_id_38 = (v_gl_id_38 + GLOBAL_SIZE_1)) {
      v__56 = calculateDiv(v__42[((v_M_0 * ( (v_gl_id_37 >= 0) ? ( (v_gl_id_37 < v_N_1) ? v_gl_id_37 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_38 >= 0) ? ( (v_gl_id_38 < v_M_0) ? v_gl_id_38 : (-1 + v_M_0) ) : 0 ))], v__42[((v_M_0 * ( ((1 + v_gl_id_37) >= 0) ? ( ((1 + v_gl_id_37) < v_N_1) ? (1 + v_gl_id_37) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_38 >= 0) ? ( (v_gl_id_38 < v_M_0) ? v_gl_id_38 : (-1 + v_M_0) ) : 0 ))], v__42[((v_M_0 * ( (v_gl_id_37 >= 0) ? ( (v_gl_id_37 < v_N_1) ? v_gl_id_37 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_38 >= 0) ? ( (v_gl_id_38 < v_M_0) ? v_gl_id_38 : (-1 + v_M_0) ) : 0 ))], v__42[((v_M_0 * ( (v_gl_id_37 >= 0) ? ( (v_gl_id_37 < v_N_1) ? v_gl_id_37 : (-1 + v_N_1) ) : 0 )) + ( ((1 + v_gl_id_38) >= 0) ? ( ((1 + v_gl_id_38) < v_M_0) ? (1 + v_gl_id_38) : (-1 + v_M_0) ) : 0 ))], v__43[(v_gl_id_38 + (v_M_0 * v_gl_id_37))], v__44[(v_gl_id_38 + (v_M_0 * v_gl_id_37))], v__47[(v_gl_id_38 + (v_M_0 * v_gl_id_37))], v__45[(v_gl_id_38 + (v_M_0 * v_gl_id_37))]); 
      v__62 = calculateImageUpdate(v__39[((v_M_0 * ( (v_gl_id_37 >= 0) ? ( (v_gl_id_37 < v_N_1) ? v_gl_id_37 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_38 >= 0) ? ( (v_gl_id_38 < v_M_0) ? v_gl_id_38 : (-1 + v_M_0) ) : 0 ))], v__56); 
      v__63[(v_gl_id_38 + (v_M_0 * v_gl_id_37))] = id(v__62); 
    }
  }
}}


