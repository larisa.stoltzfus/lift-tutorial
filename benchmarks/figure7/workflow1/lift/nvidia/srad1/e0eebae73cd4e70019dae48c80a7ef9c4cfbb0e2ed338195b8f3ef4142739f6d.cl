
// High-level hash: 8dbfc2361134047c70b7246f35bc516900cb12f5469167c7d973f5d2c40a9495
// Low-level hash: e0eebae73cd4e70019dae48c80a7ef9c4cfbb0e2ed338195b8f3ef4142739f6d

#atf::var<int> v_M_0 = 502
#atf::var<int> v_N_1 = 458

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(900)"
#atf::abort_condition "atf::cond::evaluations(1)"



#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(128,128)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(256,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(128,128)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1))"
#atf::ocl::input "atf::scalar<int>(v_M_0)"
#atf::ocl::input "atf::scalar<int>(v_N_1)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float subtract(float l, float r){
  { return l - r; }
}
float calculateScoeff(float dN, float dS, float dE, float dW, float jC, float q0sqr){
  { float g2 = (dN*dN + dS*dS + dW*dW + dE*dE) / (jC * jC);float l = (dN + dS + dW + dE ) / jC; float num = (0.5*g2) - ((1.0/16.0)*(l*l)); float  den = 1 + (0.25*l);float qsqr = num/(den*den); den = (qsqr-q0sqr) / (q0sqr * (1+q0sqr));float coeff = 1.0 / (1.0+den);  if(coeff > 1) { return 1.0f; } else if(coeff < 0 ) { return 0.0f; } else { return coeff;}  }
}
float idfloat(float x){
  { return x; }
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__15, global float* v__28, int v_M_0, int v_N_1){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__24; 
  /* Private Memory */
  float v__17; 
  float v__19; 
  float v__21; 
  float v__23; 
  float v__25; 
  float v__27; 
  for (int v_gl_id_13 = get_global_id(1); v_gl_id_13 < v_N_1; v_gl_id_13 = (v_gl_id_13 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_14 = get_global_id(0); v_gl_id_14 < v_M_0; v_gl_id_14 = (v_gl_id_14 + GLOBAL_SIZE_0)) {
      v__17 = subtract(v__15[((v_M_0 * ( ((-1 + v_gl_id_13) >= 0) ? ( ((-1 + v_gl_id_13) < v_N_1) ? (-1 + v_gl_id_13) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_0) ? v_gl_id_14 : (-1 + v_M_0) ) : 0 ))], v__15[((v_M_0 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_1) ? v_gl_id_13 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_0) ? v_gl_id_14 : (-1 + v_M_0) ) : 0 ))]); 
      v__19 = subtract(v__15[((v_M_0 * ( ((1 + v_gl_id_13) >= 0) ? ( ((1 + v_gl_id_13) < v_N_1) ? (1 + v_gl_id_13) : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_0) ? v_gl_id_14 : (-1 + v_M_0) ) : 0 ))], v__15[((v_M_0 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_1) ? v_gl_id_13 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_0) ? v_gl_id_14 : (-1 + v_M_0) ) : 0 ))]); 
      v__21 = subtract(v__15[((v_M_0 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_1) ? v_gl_id_13 : (-1 + v_N_1) ) : 0 )) + ( ((1 + v_gl_id_14) >= 0) ? ( ((1 + v_gl_id_14) < v_M_0) ? (1 + v_gl_id_14) : (-1 + v_M_0) ) : 0 ))], v__15[((v_M_0 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_1) ? v_gl_id_13 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_0) ? v_gl_id_14 : (-1 + v_M_0) ) : 0 ))]); 
      v__23 = subtract(v__15[((v_M_0 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_1) ? v_gl_id_13 : (-1 + v_N_1) ) : 0 )) + ( ((-1 + v_gl_id_14) >= 0) ? ( ((-1 + v_gl_id_14) < v_M_0) ? (-1 + v_gl_id_14) : (-1 + v_M_0) ) : 0 ))], v__15[((v_M_0 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_1) ? v_gl_id_13 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_0) ? v_gl_id_14 : (-1 + v_M_0) ) : 0 ))]); 
      float v_tmp_96 = 0.05378722f; 
      v__24 = v_tmp_96; 
      v__25 = idfloat(v__24); 
      v__27 = calculateScoeff(v__17, v__19, v__21, v__23, v__15[((v_M_0 * ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_N_1) ? v_gl_id_13 : (-1 + v_N_1) ) : 0 )) + ( (v_gl_id_14 >= 0) ? ( (v_gl_id_14 < v_M_0) ? v_gl_id_14 : (-1 + v_M_0) ) : 0 ))], v__25); 
      v__28[(v_gl_id_14 + (v_M_0 * v_gl_id_13))] = id(v__27); 
    }
  }
}}


