#!/bin/bash

for x in {1..10}; do ./run | grep SRAD; done | awk '{ srad1 += $2; srad2 += $4} END { printf srad1/10 " " srad2/10 "\n" }'
