
// High-level hash: 284ef0a6d98ad58650682e3ad7d3bac4d79f7e238545a35053188d6815088240
// Low-level hash: 0d2469f6a75b3cb1c9f21b3149a3e6f5a2dac035aafec59ad0aedd3e6162d621

#atf::var<int> v_M_129 = 4098
#atf::var<int> v_N_130 = 4098

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::evaluations(1)"


#atf::tp name "v_TP_1700" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides((-2+v_M_129))"
#atf::tp name "v_TP_1699" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides((-2+v_N_130))"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(4096,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(128,128)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1) && [&](auto LOCAL_SIZE_1){ return LOCAL_SIZE_0 * LOCAL_SIZE_1 >= 32; }"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_129*v_N_130))"
#atf::ocl::input "atf::buffer<float>(9)"
#atf::ocl::input "atf::buffer<float>((4+(-2*v_M_129)+(-2*v_N_130)+(v_M_129*v_N_130)))"
#atf::ocl::input "atf::scalar<int>(v_M_129)"
#atf::ocl::input "atf::scalar<int>(v_N_130)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct {
  float _0;
  float _1;
} Tuple2_float_float;
#endif

float idfloat(float x){
  { return x; }
}
float add(float x, float y){
  { return x+y; }
}
float mult(float l, float r){
  { return l * r; }
}
kernel void KERNEL(const global float* restrict v__1718, const global float* restrict v__1719, global float* v__1728, int v_M_129, int v_N_130){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__1720; 
  /* Private Memory */
  float v__1721_0;
  
  float v__1725_0;
  
  for (int v_gl_id_1712 = get_global_id(1); v_gl_id_1712 < ((-2 + v_N_130) / v_TP_1699); v_gl_id_1712 = (v_gl_id_1712 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_1713 = get_global_id(0); v_gl_id_1713 < ((-2 + v_M_129) / v_TP_1700); v_gl_id_1713 = (v_gl_id_1713 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_1714 = 0; v_i_1714 < v_TP_1699; v_i_1714 = (1 + v_i_1714)) {
        /* map_seq */
        for (int v_i_1715 = 0; v_i_1715 < v_TP_1700; v_i_1715 = (1 + v_i_1715)) {
          float v_tmp_1763 = 0.0f; 
          v__1720 = v_tmp_1763; 
          v__1721_0 = idfloat(v__1720); 
          /* reduce_seq */
          /* unroll */
          v__1725_0 = mult(v__1719[0], v__1718[((v_i_1715 % v_TP_1700) + (v_M_129 * v_TP_1699 * v_gl_id_1712) + (v_M_129 * ((((-2 + v_M_129) / v_TP_1700) * (v_i_1714 % (2 + v_TP_1699))) / ((-2 + v_M_129) / v_TP_1700))) + (v_M_129 * (v_gl_id_1713 / ((-2 + v_M_129) / v_TP_1700))) + (v_TP_1700 * ((v_gl_id_1713 + (((-2 + v_M_129) / v_TP_1700) * (v_i_1714 % (2 + v_TP_1699)))) % ((-2 + v_M_129) / v_TP_1700))))]); 
          v__1721_0 = add(v__1721_0, v__1725_0); 
          v__1725_0 = mult(v__1719[1], v__1718[(1 + (v_i_1715 % v_TP_1700) + (v_M_129 * v_TP_1699 * v_gl_id_1712) + (v_M_129 * ((((-2 + v_M_129) / v_TP_1700) * (v_i_1714 % (2 + v_TP_1699))) / ((-2 + v_M_129) / v_TP_1700))) + (v_M_129 * (v_gl_id_1713 / ((-2 + v_M_129) / v_TP_1700))) + (v_TP_1700 * ((v_gl_id_1713 + (((-2 + v_M_129) / v_TP_1700) * (v_i_1714 % (2 + v_TP_1699)))) % ((-2 + v_M_129) / v_TP_1700))))]); 
          v__1721_0 = add(v__1721_0, v__1725_0); 
          v__1725_0 = mult(v__1719[2], v__1718[(2 + (v_i_1715 % v_TP_1700) + (v_M_129 * v_TP_1699 * v_gl_id_1712) + (v_M_129 * ((((-2 + v_M_129) / v_TP_1700) * (v_i_1714 % (2 + v_TP_1699))) / ((-2 + v_M_129) / v_TP_1700))) + (v_M_129 * (v_gl_id_1713 / ((-2 + v_M_129) / v_TP_1700))) + (v_TP_1700 * ((v_gl_id_1713 + (((-2 + v_M_129) / v_TP_1700) * (v_i_1714 % (2 + v_TP_1699)))) % ((-2 + v_M_129) / v_TP_1700))))]); 
          v__1721_0 = add(v__1721_0, v__1725_0); 
          v__1725_0 = mult(v__1719[3], v__1718[(((v_TP_1700 + v_i_1715) % v_TP_1700) + (v_M_129 * v_TP_1699 * v_gl_id_1712) + (v_M_129 * (v_gl_id_1713 / ((-2 + v_M_129) / v_TP_1700))) + (v_M_129 * ((((-2 + v_M_129) / v_TP_1700) * ((1 + v_i_1714) % (2 + v_TP_1699))) / ((-2 + v_M_129) / v_TP_1700))) + (v_TP_1700 * ((v_gl_id_1713 + (((-2 + v_M_129) / v_TP_1700) * ((1 + v_i_1714) % (2 + v_TP_1699)))) % ((-2 + v_M_129) / v_TP_1700))))]); 
          v__1721_0 = add(v__1721_0, v__1725_0); 
          v__1725_0 = mult(v__1719[4], v__1718[(1 + ((v_TP_1700 + v_i_1715) % v_TP_1700) + (v_M_129 * v_TP_1699 * v_gl_id_1712) + (v_M_129 * (v_gl_id_1713 / ((-2 + v_M_129) / v_TP_1700))) + (v_M_129 * ((((-2 + v_M_129) / v_TP_1700) * ((1 + v_i_1714) % (2 + v_TP_1699))) / ((-2 + v_M_129) / v_TP_1700))) + (v_TP_1700 * ((v_gl_id_1713 + (((-2 + v_M_129) / v_TP_1700) * ((1 + v_i_1714) % (2 + v_TP_1699)))) % ((-2 + v_M_129) / v_TP_1700))))]); 
          v__1721_0 = add(v__1721_0, v__1725_0); 
          v__1725_0 = mult(v__1719[5], v__1718[(2 + ((v_TP_1700 + v_i_1715) % v_TP_1700) + (v_M_129 * v_TP_1699 * v_gl_id_1712) + (v_M_129 * (v_gl_id_1713 / ((-2 + v_M_129) / v_TP_1700))) + (v_M_129 * ((((-2 + v_M_129) / v_TP_1700) * ((1 + v_i_1714) % (2 + v_TP_1699))) / ((-2 + v_M_129) / v_TP_1700))) + (v_TP_1700 * ((v_gl_id_1713 + (((-2 + v_M_129) / v_TP_1700) * ((1 + v_i_1714) % (2 + v_TP_1699)))) % ((-2 + v_M_129) / v_TP_1700))))]); 
          v__1721_0 = add(v__1721_0, v__1725_0); 
          v__1725_0 = mult(v__1719[6], v__1718[(((v_i_1715 + (2 * v_TP_1700)) % v_TP_1700) + (v_M_129 * v_TP_1699 * v_gl_id_1712) + (v_M_129 * (v_gl_id_1713 / ((-2 + v_M_129) / v_TP_1700))) + (v_M_129 * ((((-2 + v_M_129) / v_TP_1700) * ((2 + v_i_1714) % (2 + v_TP_1699))) / ((-2 + v_M_129) / v_TP_1700))) + (v_TP_1700 * ((v_gl_id_1713 + (((-2 + v_M_129) / v_TP_1700) * ((2 + v_i_1714) % (2 + v_TP_1699)))) % ((-2 + v_M_129) / v_TP_1700))))]); 
          v__1721_0 = add(v__1721_0, v__1725_0); 
          v__1725_0 = mult(v__1719[7], v__1718[(1 + ((v_i_1715 + (2 * v_TP_1700)) % v_TP_1700) + (v_M_129 * v_TP_1699 * v_gl_id_1712) + (v_M_129 * (v_gl_id_1713 / ((-2 + v_M_129) / v_TP_1700))) + (v_M_129 * ((((-2 + v_M_129) / v_TP_1700) * ((2 + v_i_1714) % (2 + v_TP_1699))) / ((-2 + v_M_129) / v_TP_1700))) + (v_TP_1700 * ((v_gl_id_1713 + (((-2 + v_M_129) / v_TP_1700) * ((2 + v_i_1714) % (2 + v_TP_1699)))) % ((-2 + v_M_129) / v_TP_1700))))]); 
          v__1721_0 = add(v__1721_0, v__1725_0); 
          v__1725_0 = mult(v__1719[8], v__1718[(2 + ((v_i_1715 + (2 * v_TP_1700)) % v_TP_1700) + (v_M_129 * v_TP_1699 * v_gl_id_1712) + (v_M_129 * (v_gl_id_1713 / ((-2 + v_M_129) / v_TP_1700))) + (v_M_129 * ((((-2 + v_M_129) / v_TP_1700) * ((2 + v_i_1714) % (2 + v_TP_1699))) / ((-2 + v_M_129) / v_TP_1700))) + (v_TP_1700 * ((v_gl_id_1713 + (((-2 + v_M_129) / v_TP_1700) * ((2 + v_i_1714) % (2 + v_TP_1699)))) % ((-2 + v_M_129) / v_TP_1700))))]); 
          v__1721_0 = add(v__1721_0, v__1725_0); 
          /* end unroll */
          /* end reduce_seq */
          /* map_seq */
          /* unroll */
          v__1728[(v_i_1715 + (-2 * v_TP_1699 * v_gl_id_1712) + (-2 * ((((-2 + v_M_129) / v_TP_1700) * ((v_i_1714 + (v_TP_1699 * v_gl_id_1713)) % v_TP_1699)) / ((-2 + v_M_129) / v_TP_1700))) + (v_M_129 * ((((-2 + v_M_129) / v_TP_1700) * ((v_i_1714 + (v_TP_1699 * v_gl_id_1713)) % v_TP_1699)) / ((-2 + v_M_129) / v_TP_1700))) + (-2 * (v_gl_id_1713 / ((-2 + v_M_129) / v_TP_1700))) + (v_M_129 * v_TP_1699 * v_gl_id_1712) + (v_M_129 * (v_gl_id_1713 / ((-2 + v_M_129) / v_TP_1700))) + (v_TP_1700 * ((v_gl_id_1713 + (((-2 + v_M_129) / v_TP_1700) * ((v_i_1714 + (v_TP_1699 * v_gl_id_1713)) % v_TP_1699))) % ((-2 + v_M_129) / v_TP_1700))))] = idfloat(v__1721_0); 
          /* end unroll */
          /* end map_seq */
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


