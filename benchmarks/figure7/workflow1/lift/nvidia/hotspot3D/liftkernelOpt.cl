float calculateHotspot(float tInC, float cc, float tInN, float cn, float tInS, float cs, float tInE, float ce, float tInW, float cw, float tInT, float ct, float tInB, float cb, float stepDivCap, float pInC, float amb_temp){
  { return  tInC*cc + tInN*cn + tInS*cs + tInE*ce + tInW*cw + tInT*ct + tInB*cb + stepDivCap * pInC + ct*amb_temp; }
}
float id(float x){
  { return x; }
}

kernel void hotspotOpt1(const global float* restrict v__58, const global float* restrict v__59, float v__60, float v__61, float v__62, float v__63, float v__64, float v__65, float v__66, float v__67, global float* v__75, int v_M_3, int v_N_4, int v_O_5){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__72;
  /* Private Memory */
  float v__74;
  for (int v_gl_id_55 = get_global_id(2);v_gl_id_55<v_O_5;v_gl_id_55 = (8 + v_gl_id_55)){
    for (int v_gl_id_56 = get_global_id(1);v_gl_id_56<v_N_4;v_gl_id_56 = (512 + v_gl_id_56)){
      for (int v_gl_id_57 = get_global_id(0);v_gl_id_57<v_M_3;v_gl_id_57 = (512 + v_gl_id_57)){
        float v_tmp_84 = 80.0f;
        v__72 = v_tmp_84;
        v__74 = calculateHotspot(v__58[((v_M_3 * v_N_4 * ( (v_gl_id_55 >= 0) ? ( (v_gl_id_55 < v_O_5) ? v_gl_id_55 : (-1 + (2 * v_O_5) + (-1 * v_gl_id_55)) ) : (-1 + (-1 * v_gl_id_55)) )) + (v_M_3 * ( (v_gl_id_56 >= 0) ? ( (v_gl_id_56 < v_N_4) ? v_gl_id_56 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_56)) ) : (-1 + (-1 * v_gl_id_56)) )) + ( (v_gl_id_57 >= 0) ? ( (v_gl_id_57 < v_M_3) ? v_gl_id_57 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_57)) ) : (-1 + (-1 * v_gl_id_57)) ))], v__66, v__58[((v_M_3 * v_N_4 * ( (v_gl_id_55 >= 0) ? ( (v_gl_id_55 < v_O_5) ? v_gl_id_55 : (-1 + (2 * v_O_5) + (-1 * v_gl_id_55)) ) : (-1 + (-1 * v_gl_id_55)) )) + (v_M_3 * ( ((-1 + v_gl_id_56) >= 0) ? ( ((-1 + v_gl_id_56) < v_N_4) ? (-1 + v_gl_id_56) : ((-1 * v_gl_id_56) + (2 * v_N_4)) ) : (-1 * v_gl_id_56) )) + ( (v_gl_id_57 >= 0) ? ( (v_gl_id_57 < v_M_3) ? v_gl_id_57 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_57)) ) : (-1 + (-1 * v_gl_id_57)) ))], v__62, v__58[((v_M_3 * v_N_4 * ( (v_gl_id_55 >= 0) ? ( (v_gl_id_55 < v_O_5) ? v_gl_id_55 : (-1 + (2 * v_O_5) + (-1 * v_gl_id_55)) ) : (-1 + (-1 * v_gl_id_55)) )) + (v_M_3 * ( ((1 + v_gl_id_56) >= 0) ? ( ((1 + v_gl_id_56) < v_N_4) ? (1 + v_gl_id_56) : (-2 + (2 * v_N_4) + (-1 * v_gl_id_56)) ) : (-2 + (-1 * v_gl_id_56)) )) + ( (v_gl_id_57 >= 0) ? ( (v_gl_id_57 < v_M_3) ? v_gl_id_57 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_57)) ) : (-1 + (-1 * v_gl_id_57)) ))], v__63, v__58[((v_M_3 * v_N_4 * ( ((1 + v_gl_id_55) >= 0) ? ( ((1 + v_gl_id_55) < v_O_5) ? (1 + v_gl_id_55) : (-2 + (2 * v_O_5) + (-1 * v_gl_id_55)) ) : (-2 + (-1 * v_gl_id_55)) )) + (v_M_3 * ( (v_gl_id_56 >= 0) ? ( (v_gl_id_56 < v_N_4) ? v_gl_id_56 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_56)) ) : (-1 + (-1 * v_gl_id_56)) )) + ( (v_gl_id_57 >= 0) ? ( (v_gl_id_57 < v_M_3) ? v_gl_id_57 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_57)) ) : (-1 + (-1 * v_gl_id_57)) ))], v__60, v__58[((v_M_3 * v_N_4 * ( ((-1 + v_gl_id_55) >= 0) ? ( ((-1 + v_gl_id_55) < v_O_5) ? (-1 + v_gl_id_55) : ((-1 * v_gl_id_55) + (2 * v_O_5)) ) : (-1 * v_gl_id_55) )) + (v_M_3 * ( (v_gl_id_56 >= 0) ? ( (v_gl_id_56 < v_N_4) ? v_gl_id_56 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_56)) ) : (-1 + (-1 * v_gl_id_56)) )) + ( (v_gl_id_57 >= 0) ? ( (v_gl_id_57 < v_M_3) ? v_gl_id_57 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_57)) ) : (-1 + (-1 * v_gl_id_57)) ))], v__61, v__58[((v_M_3 * v_N_4 * ( (v_gl_id_55 >= 0) ? ( (v_gl_id_55 < v_O_5) ? v_gl_id_55 : (-1 + (2 * v_O_5) + (-1 * v_gl_id_55)) ) : (-1 + (-1 * v_gl_id_55)) )) + (v_M_3 * ( (v_gl_id_56 >= 0) ? ( (v_gl_id_56 < v_N_4) ? v_gl_id_56 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_56)) ) : (-1 + (-1 * v_gl_id_56)) )) + ( ((1 + v_gl_id_57) >= 0) ? ( ((1 + v_gl_id_57) < v_M_3) ? (1 + v_gl_id_57) : (-2 + (2 * v_M_3) + (-1 * v_gl_id_57)) ) : (-2 + (-1 * v_gl_id_57)) ))], v__64, v__58[((v_M_3 * v_N_4 * ( (v_gl_id_55 >= 0) ? ( (v_gl_id_55 < v_O_5) ? v_gl_id_55 : (-1 + (2 * v_O_5) + (-1 * v_gl_id_55)) ) : (-1 + (-1 * v_gl_id_55)) )) + (v_M_3 * ( (v_gl_id_56 >= 0) ? ( (v_gl_id_56 < v_N_4) ? v_gl_id_56 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_56)) ) : (-1 + (-1 * v_gl_id_56)) )) + ( ((-1 + v_gl_id_57) >= 0) ? ( ((-1 + v_gl_id_57) < v_M_3) ? (-1 + v_gl_id_57) : ((-1 * v_gl_id_57) + (2 * v_M_3)) ) : (-1 * v_gl_id_57) ))], v__65, v__67, v__59[(v_gl_id_57 + (v_M_3 * v_N_4 * v_gl_id_55) + (v_M_3 * v_gl_id_56))], v__72);
        v__75[(v_gl_id_57 + (v_M_3 * v_N_4 * v_gl_id_55) + (v_M_3 * v_gl_id_56))] = id(v__74);
      }
    }
  }
}}
