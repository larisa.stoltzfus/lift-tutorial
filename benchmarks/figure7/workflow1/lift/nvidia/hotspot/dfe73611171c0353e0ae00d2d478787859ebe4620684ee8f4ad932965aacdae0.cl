
// High-level hash: 021c44fe650965f5b445e1932db08add744d2ebbf24cc3444cb039d438f2337c
// Low-level hash: dfe73611171c0353e0ae00d2d478787859ebe4620684ee8f4ad932965aacdae0

#atf::var<int> v_M_56 = 8192
#atf::var<int> v_N_57 = 8192

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::abort_condition "atf::cond::evaluations(1)"


#atf::tp name "v_TP_78" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(v_M_56)"
#atf::tp name "v_TP_77" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(v_N_57)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(8192,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(32,32)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_1) && [&](auto LOCAL_SIZE_1){ return LOCAL_SIZE_0 * LOCAL_SIZE_1 >= 32; }"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_56*v_N_57))"
#atf::ocl::input "atf::buffer<float>((v_M_56*v_N_57))"
#atf::ocl::input "atf::buffer<float>((v_M_56*v_N_57))"
#atf::ocl::input "atf::scalar<int>(v_M_56)"
#atf::ocl::input "atf::scalar<int>(v_N_57)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float hotspot(float power, float top, float bottom, float left, float right, float center){
  
#define MAX_PD  (3.0e6)
#define PRECISION   0.001
#define SPEC_HEAT_SI 1.75e6
#define K_SI 100
#define FACTOR_CHIP 0.5

    /* chip parameters  */
    const float t_chip = 0.0005f;
    const float chip_height = 0.016f;
    const float chip_width = 0.016f;
    /* ambient temperature, assuming no package at all  */
    const float amb_temp = 80.0f;

    float row = 512.0f;
    float col = 512.0f;

    float grid_height = chip_height / row;
    float grid_width = chip_width / col;

    float Cap = FACTOR_CHIP * SPEC_HEAT_SI * t_chip * grid_width * grid_height;
    float Rx = grid_width / (2.0 * K_SI * t_chip * grid_height);
    float Ry = grid_height / (2.0 * K_SI * t_chip * grid_width);
    float Rz = t_chip / (K_SI * grid_height * grid_width);

    float max_slope = MAX_PD / (FACTOR_CHIP * t_chip * SPEC_HEAT_SI);
    float stepl = PRECISION / max_slope;

    float step_div_Cap=stepl/Cap;
    float Rx_1=1/Rx;
    float Ry_1=1/Ry;
    float Rz_1=1/Rz;

    return center +
       step_div_Cap * (power + (bottom + top - 2.0f * center) * Ry_1 +
               (right + left - 2.0f * center) * Rx_1 + (amb_temp - center) * Rz_1);

    
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__95, const global float* restrict v__96, global float* v__103, int v_M_56, int v_N_57){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__102_0;
  
  for (int v_gl_id_91 = get_global_id(1); v_gl_id_91 < (v_N_57 / v_TP_77); v_gl_id_91 = (v_gl_id_91 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_92 = get_global_id(0); v_gl_id_92 < (v_M_56 / v_TP_78); v_gl_id_92 = (v_gl_id_92 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_93 = 0; v_i_93 < v_TP_77; v_i_93 = (1 + v_i_93)) {
        /* map_seq */
        for (int v_i_94 = 0; v_i_94 < v_TP_78; v_i_94 = (1 + v_i_94)) {
          v__102_0 = hotspot(v__96[(v_i_94 + (v_M_56 * v_TP_77 * v_gl_id_91) + (v_M_56 * (v_gl_id_92 / (v_M_56 / v_TP_78))) + (v_M_56 * (((v_M_56 / v_TP_78) * ((v_i_93 + (v_TP_77 * v_gl_id_92)) % v_TP_77)) / (v_M_56 / v_TP_78))) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((v_i_93 + (v_TP_77 * v_gl_id_92)) % v_TP_77))) % (v_M_56 / v_TP_78))))], v__95[((v_M_56 * ( ((-1 + (((v_M_56 / v_TP_78) * (v_i_93 % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) >= 0) ? ( ((-1 + (((v_M_56 / v_TP_78) * (v_i_93 % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) < v_N_57) ? (-1 + (((v_M_56 / v_TP_78) * (v_i_93 % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) : (-1 + v_N_57) ) : 0 )) + ( (((v_i_94 % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * (v_i_93 % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) >= 0) ? ( (((v_i_94 % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * (v_i_93 % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) < v_M_56) ? ((v_i_94 % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * (v_i_93 % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) : (-1 + v_M_56) ) : 0 ))], v__95[((v_M_56 * ( ((-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((2 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) >= 0) ? ( ((-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((2 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) < v_N_57) ? (-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((2 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) : (-1 + v_N_57) ) : 0 )) + ( ((((v_i_94 + (2 * v_TP_78)) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((2 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) >= 0) ? ( ((((v_i_94 + (2 * v_TP_78)) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((2 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) < v_M_56) ? (((v_i_94 + (2 * v_TP_78)) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((2 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) : (-1 + v_M_56) ) : 0 ))], v__95[((v_M_56 * ( ((-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) >= 0) ? ( ((-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) < v_N_57) ? (-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) : (-1 + v_N_57) ) : 0 )) + ( ((-1 + ((v_TP_78 + v_i_94) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) >= 0) ? ( ((-1 + ((v_TP_78 + v_i_94) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) < v_M_56) ? (-1 + ((v_TP_78 + v_i_94) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) : (-1 + v_M_56) ) : 0 ))], v__95[((v_M_56 * ( ((-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) >= 0) ? ( ((-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) < v_N_57) ? (-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) : (-1 + v_N_57) ) : 0 )) + ( ((1 + ((v_TP_78 + v_i_94) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) >= 0) ? ( ((1 + ((v_TP_78 + v_i_94) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) < v_M_56) ? (1 + ((v_TP_78 + v_i_94) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) : (-1 + v_M_56) ) : 0 ))], v__95[((v_M_56 * ( ((-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) >= 0) ? ( ((-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) < v_N_57) ? (-1 + (v_gl_id_92 / (v_M_56 / v_TP_78)) + (((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77))) / (v_M_56 / v_TP_78)) + (v_TP_77 * v_gl_id_91)) : (-1 + v_N_57) ) : 0 )) + ( ((((v_TP_78 + v_i_94) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) >= 0) ? ( ((((v_TP_78 + v_i_94) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) < v_M_56) ? (((v_TP_78 + v_i_94) % v_TP_78) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((1 + v_i_93) % (2 + v_TP_77)))) % (v_M_56 / v_TP_78)))) : (-1 + v_M_56) ) : 0 ))]); 
          v__103[(v_i_94 + (v_M_56 * v_TP_77 * v_gl_id_91) + (v_M_56 * (v_gl_id_92 / (v_M_56 / v_TP_78))) + (v_M_56 * (((v_M_56 / v_TP_78) * ((v_i_93 + (v_TP_77 * v_gl_id_92)) % v_TP_77)) / (v_M_56 / v_TP_78))) + (v_TP_78 * ((v_gl_id_92 + ((v_M_56 / v_TP_78) * ((v_i_93 + (v_TP_77 * v_gl_id_92)) % v_TP_77))) % (v_M_56 / v_TP_78))))] = id(v__102_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


