#!/bin/bash

DIR=$(pwd)
FILENAME=$(basename "${DIR}")
OUTDATA="${FILENAME}.out"

# clean atfc/build
cd $ATF/build
rm *.cl *.csv
cd -
cp *.cl $ATF/build
cd $ATF/build
START=1
END=$ITERATIONS
for i in $(eval echo "{$START..$END}")
do
				./atfc -I -i *.cl					
done
awk 'NR % 2 == 0' cost.csv | sed 's/;/ /g' | awk '{ print $NF }' > $OUTDATA
cd -
mv $ATF/build/$OUTDATA .
