#pragma OPENCL EXTENSION cl_khr_fp64 : enable

typedef float value;

#define area (Nx*Ny)


// Define Source and Read


typedef struct coeffs_type
{
	value l2;
	value loss1;
	value loss2;

} coeffs_type;

__kernel void UpdateScheme(__global value *u,
                           __global value *u1, 
                           __constant struct coeffs_type* cf_d)
{
	
	// get X,Y from thread and block Id - tiling along Z
	int X = get_global_id(0); 
	int Y = get_global_id(1); 


	int cp = area+(Y*Nx+X);


        value sum = 0.0; 
        value l2 = cf_d[0].l2;
        value loss1 = cf_d[0].loss1;
        value loss2 = cf_d[0].loss2;
        value cf = 1.0;
        value cf2 = 1.0; 

        
	int Z;
        __local value localGrid1[BLOCK_X+2][BLOCK_Y+2];
        value cpAreaP = 0.0; 
        value cpArea = u1[cp]; 
        value cpAreaM = 0.0; 
        
        // loop over the Z index 
        for(Z=1;Z<(Nz-1);Z++)
        {

           cp = Z*area+(Y*Nx+X);
                
                cpAreaP=u1[cp+area];

                // setup local memory XY grid 
       	        int localX = get_local_id(0); 
                int localY = get_local_id(1); 
    
                localX++;
                localY++;

                localGrid1[localX][localY] = cpArea;  

                if((localX==1) && !(X==0)){ 
                    localGrid1[localX-1][localY]=u1[cp-1];
                }
                if((localX==BLOCK_X) && !(X==(Nx-1))){ 
                    localGrid1[localX+1][localY]=u1[cp+1];
                }

                if((localY==1) && !(Y==0)){ 
                    localGrid1[localX][localY-1]=u1[cp-Nx];
                }

                if((localY==BLOCK_Y) && !(Y==(Ny-1))){ 
                    localGrid1[localX][localY+1]=u1[cp+Nx];
                }

               barrier(CLK_LOCAL_MEM_FENCE);

                if( (X>0) && (X<(Nx-1)) && (Y>0) && (Y<(Ny-1))){
		// get linear position
                    sum = localGrid1[localX-1][localY]+localGrid1[localX+1][localY]+localGrid1[localX][localY-1]+localGrid1[localX][localY+1]+cpAreaP+cpAreaM;
		
		    // local variables

        	    int K    = (0||(X-1)) + (0||(X-(Nx-2))) + (0||(Y-1)) + (0||(Y-(Ny-2))) + (0||(Z-1)) + (0||(Z-(Nz-2)));
		
                    cf  = 1.0;
                    cf2 = 1.0;
                    // set loss coeffs at walls
                    if(K<6){
			cf   = loss1; 
			cf2  = loss2;
		    } 
        
                    // Calc update
                    u[cp]    = cf*( (2.0-K*cf_d[0].l2)*cpArea + cf_d[0].l2*sum - cf2*u[cp] );

                    // swap values for next iteration
                    cpAreaM = cpArea;
                    cpArea = cpAreaP;
                    barrier(CLK_LOCAL_MEM_FENCE);
                }
	    	
	}
	
}

