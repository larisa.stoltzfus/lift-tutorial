#!/bin/bash
DIR=$(pwd)
FILENAME=$(basename "${DIR}")

if [ "$#" -ne 4 ]; then
				echo "Illegal number of parameters (enter platform, device, input size and number of iterations)"
                                echo "(IE: ./benchmark.sh 0 0 512 1)"
				exit -1
fi

N=$4

RAWDATA="${FILENAME}.raw"
OUTDATA="${FILENAME}.out"
if [ -e $RAWDATA ]
then
    rm $RAWDATA 
fi
if [ -e $OUTDATA ]
then
    rm $OUTDATA 
fi

for i in $(seq 1 $N)
do
./hotspot $3 2 1 ${DATADIR}/hotspot/temp_$3 $DATADIR/hotspot/power_$3 output.out $1 $2 >> $RAWDATA
echo "./hotspot $3 2 1 ${DATADIR}/hotspot/temp_$3 $DATADIR/hotspot/power_$3 output.out $1 $2 >> $RAWDATA"
done

cat $RAWDATA | grep DEBUG | awk '{print $3}' > $OUTDATA
cat $OUTDATA
