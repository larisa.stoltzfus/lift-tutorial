#!/bin/bash


DIR=$(pwd)
FILENAME=$(basename "${DIR}")

if [ "$#" -ne 5 ]; then
				echo "Illegal number of parameters (enter platform, device, input size, layers and iterations)"
                                echo "(IE. ./benchmark.sh 0 0 512 8 1)"
				exit -1
fi

N=$5

RAWDATA="${FILENAME}.raw"
OUTDATA="${FILENAME}.out"

if [ -e $RAWDATA ]
then
    rm $RAWDATA
fi
if [ -e $OUTDATA ]
then
    rm $OUTDATA
fi

#echo ./3D $3 $4 $5 $DATADIR/hotspot3D/power_$3x$4 $DATADIR/hotspot3D/temp_$3x$4  output.out $1 $2 

for i in $(seq 1 $N)
do
    ./3D $3 $4 1 $DATADIR/hotspot3D/power_$3x$4 $DATADIR/hotspot3D/temp_$3x$4  output.out 0 0 >> hotspot3D.raw
done

cat $RAWDATA | grep DEBUG | awk '{print $3}' > $OUTDATA
cat $OUTDATA
