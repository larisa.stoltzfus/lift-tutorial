
// High-level hash: 7b1586687b50546015b3162e1e3abfb4f73f61833fc59ffcd7b8b33cfda3da2d
// Low-level hash: 1b78677663bd2dedfca1d511f2bb6b08eb5a0c873e51762fac94c9f7387371c7

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__14, global float* v__17){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__16; 
  for (int v_gl_id_12 = get_global_id(1); v_gl_id_12 < 4094; v_gl_id_12 = (v_gl_id_12 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_13 = get_global_id(0); v_gl_id_13 < 4094; v_gl_id_13 = (v_gl_id_13 + GLOBAL_SIZE_0)) {
      v__16 = jacobi(v__14[(v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(1 + v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(2 + v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(4096 + v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(4097 + v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(4098 + v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(8192 + v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(8193 + v_gl_id_13 + (4096 * v_gl_id_12))], v__14[(8194 + v_gl_id_13 + (4096 * v_gl_id_12))]); 
      v__17[(4097 + v_gl_id_13 + (4096 * v_gl_id_12))] = id(v__16); 
    }
  }
}}


