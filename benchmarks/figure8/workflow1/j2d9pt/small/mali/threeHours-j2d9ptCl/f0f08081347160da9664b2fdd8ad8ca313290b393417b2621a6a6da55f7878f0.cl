
// High-level hash: 7b1586687b50546015b3162e1e3abfb4f73f61833fc59ffcd7b8b33cfda3da2d
// Low-level hash: f0f08081347160da9664b2fdd8ad8ca313290b393417b2621a6a6da55f7878f0

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__24, global float* v__29){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__28; 
  for (int v_gl_id_21 = get_global_id(0); v_gl_id_21 < 4094; v_gl_id_21 = (v_gl_id_21 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_23 = get_global_id(1); v_gl_id_23 < 4094; v_gl_id_23 = (v_gl_id_23 + GLOBAL_SIZE_1)) {
      v__28 = jacobi(v__24[(v_gl_id_23 + (4096 * v_gl_id_21))], v__24[(1 + v_gl_id_23 + (4096 * v_gl_id_21))], v__24[(2 + v_gl_id_23 + (4096 * v_gl_id_21))], v__24[(4096 + v_gl_id_23 + (4096 * v_gl_id_21))], v__24[(4097 + v_gl_id_23 + (4096 * v_gl_id_21))], v__24[(4098 + v_gl_id_23 + (4096 * v_gl_id_21))], v__24[(8192 + v_gl_id_23 + (4096 * v_gl_id_21))], v__24[(8193 + v_gl_id_23 + (4096 * v_gl_id_21))], v__24[(8194 + v_gl_id_23 + (4096 * v_gl_id_21))]); 
      v__29[(4097 + v_gl_id_23 + (4096 * v_gl_id_21))] = id(v__28); 
    }
  }
}}


