
// High-level hash: 240eaceda3c42cdd13f7e09bcaa2ed2c945d4005470ea7188d3cf3c3e8d75d40
// Low-level hash: c755873cf1ad37502bd392f430ad8f140ca37f2a83e3dbc8aa3333030226fd55

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_65" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_64" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__133, global float* v__144){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__141[(4+(2*v_TP_64)+(2*v_TP_65)+(v_TP_64*v_TP_65))];
  /* Typed Value memory */
  /* Private Memory */
  float v__143_0;
  
  for (int v_wg_id_125 = get_group_id(1); v_wg_id_125 < (4094 / v_TP_64); v_wg_id_125 = (v_wg_id_125 + NUM_GROUPS_1)) {
    for (int v_wg_id_126 = get_group_id(0); v_wg_id_126 < (4094 / v_TP_65); v_wg_id_126 = (v_wg_id_126 + NUM_GROUPS_0)) {
      for (int v_l_id_128 = get_local_id(1); v_l_id_128 < (2 + v_TP_64); v_l_id_128 = (v_l_id_128 + LOCAL_SIZE_1)) {
        for (int v_l_id_129 = get_local_id(0); v_l_id_129 < (2 + v_TP_65); v_l_id_129 = (v_l_id_129 + LOCAL_SIZE_0)) {
          v__141[(v_l_id_129 + (2 * v_l_id_128) + (v_TP_65 * v_l_id_128))] = idfloat(v__133[(v_l_id_129 + (4096 * v_TP_64 * v_wg_id_125) + (4096 * (v_wg_id_126 / (4094 / v_TP_65))) + (4096 * (((4094 / v_TP_65) * (v_l_id_128 % (2 + v_TP_64))) / (4094 / v_TP_65))) + (v_TP_65 * ((v_wg_id_126 + ((4094 / v_TP_65) * (v_l_id_128 % (2 + v_TP_64)))) % (4094 / v_TP_65))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_130 = get_local_id(1); v_l_id_130 < v_TP_64; v_l_id_130 = (v_l_id_130 + LOCAL_SIZE_1)) {
        for (int v_l_id_132 = get_local_id(0); v_l_id_132 < v_TP_65; v_l_id_132 = (v_l_id_132 + LOCAL_SIZE_0)) {
          v__143_0 = jacobi(v__141[((v_l_id_132 % v_TP_65) + (2 * v_l_id_130) + (v_TP_65 * v_l_id_130))], v__141[(1 + (v_l_id_132 % v_TP_65) + (2 * v_l_id_130) + (v_TP_65 * v_l_id_130))], v__141[(2 + (v_l_id_132 % v_TP_65) + (2 * v_l_id_130) + (v_TP_65 * v_l_id_130))], v__141[(2 + v_TP_65 + ((v_TP_65 + v_l_id_132) % v_TP_65) + (2 * v_l_id_130) + (v_TP_65 * v_l_id_130))], v__141[(3 + v_TP_65 + ((v_TP_65 + v_l_id_132) % v_TP_65) + (2 * v_l_id_130) + (v_TP_65 * v_l_id_130))], v__141[(4 + v_TP_65 + ((v_TP_65 + v_l_id_132) % v_TP_65) + (2 * v_l_id_130) + (v_TP_65 * v_l_id_130))], v__141[(4 + v_l_id_132 + (2 * v_TP_65) + (2 * v_l_id_130) + (v_TP_65 * v_l_id_130))], v__141[(5 + v_l_id_132 + (2 * v_TP_65) + (2 * v_l_id_130) + (v_TP_65 * v_l_id_130))], v__141[(6 + v_l_id_132 + (2 * v_TP_65) + (2 * v_l_id_130) + (v_TP_65 * v_l_id_130))]); 
          v__144[(4097 + v_l_id_132 + (-4096 * ((v_l_id_132 + (v_TP_65 * ((v_wg_id_126 + ((4094 / v_TP_65) * ((v_l_id_130 + (v_TP_64 * v_wg_id_126)) % v_TP_64))) % (4094 / v_TP_65)))) / 4095)) + (4096 * v_TP_64 * v_wg_id_125) + (-16777216 * (((((4094 / v_TP_65) * ((v_l_id_130 + (v_TP_64 * v_wg_id_126)) % v_TP_64)) / (4094 / v_TP_65)) + (v_wg_id_126 / (4094 / v_TP_65)) + (v_TP_64 * v_wg_id_125)) / 4095)) + (4096 * (((4094 / v_TP_65) * ((v_l_id_130 + (v_TP_64 * v_wg_id_126)) % v_TP_64)) / (4094 / v_TP_65))) + (4096 * (v_wg_id_126 / (4094 / v_TP_65))) + (v_TP_65 * ((v_wg_id_126 + ((4094 / v_TP_65) * ((v_l_id_130 + (v_TP_64 * v_wg_id_126)) % v_TP_64))) % (4094 / v_TP_65))))] = id(v__143_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


