{ kernel[i0] -> grid[o0, o1] : i0 = 0 and o0 = 256 and o1 = 256; kernel[i0] -> tile[o0, o1] : i0 = 0 and o0 = 32 and o1 = 32; kernel[i0] -> block[o0, o1] : i0 = 0 and o0 = 32 and o1 = 16 }

size_t global_work_size[2] = {(128) * 32, (128) * 16};
size_t block_size[2] = {32, 16};
