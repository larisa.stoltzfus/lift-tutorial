
// High-level hash: c125ef912061758c481ab314a9fcb215e5eb2a2d71057421e8b4079bb95f0097
// Low-level hash: c8e0e685b7995c493dae11781b896fca39e2cd4e58fba98a3f4ec1bd32297a42

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_294" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_293" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__331, global float* v__335){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__334_0;
  
  for (int v_wg_id_327 = get_group_id(1); v_wg_id_327 < (4094 / v_TP_293); v_wg_id_327 = (v_wg_id_327 + NUM_GROUPS_1)) {
    for (int v_wg_id_328 = get_group_id(0); v_wg_id_328 < (4094 / v_TP_294); v_wg_id_328 = (v_wg_id_328 + NUM_GROUPS_0)) {
      for (int v_l_id_329 = get_local_id(1); v_l_id_329 < v_TP_293; v_l_id_329 = (v_l_id_329 + LOCAL_SIZE_1)) {
        for (int v_l_id_330 = get_local_id(0); v_l_id_330 < v_TP_294; v_l_id_330 = (v_l_id_330 + LOCAL_SIZE_0)) {
          v__334_0 = jacobi(v__331[((v_l_id_330 % v_TP_294) + (4096 * v_TP_293 * v_wg_id_327) + (4096 * (v_wg_id_328 / (4094 / v_TP_294))) + (4096 * (((4094 / v_TP_294) * (v_l_id_329 % (2 + v_TP_293))) / (4094 / v_TP_294))) + (v_TP_294 * ((v_wg_id_328 + ((4094 / v_TP_294) * (v_l_id_329 % (2 + v_TP_293)))) % (4094 / v_TP_294))))], v__331[(1 + (v_l_id_330 % v_TP_294) + (4096 * v_TP_293 * v_wg_id_327) + (4096 * (v_wg_id_328 / (4094 / v_TP_294))) + (4096 * (((4094 / v_TP_294) * (v_l_id_329 % (2 + v_TP_293))) / (4094 / v_TP_294))) + (v_TP_294 * ((v_wg_id_328 + ((4094 / v_TP_294) * (v_l_id_329 % (2 + v_TP_293)))) % (4094 / v_TP_294))))], v__331[(2 + (v_l_id_330 % v_TP_294) + (4096 * v_TP_293 * v_wg_id_327) + (4096 * (v_wg_id_328 / (4094 / v_TP_294))) + (4096 * (((4094 / v_TP_294) * (v_l_id_329 % (2 + v_TP_293))) / (4094 / v_TP_294))) + (v_TP_294 * ((v_wg_id_328 + ((4094 / v_TP_294) * (v_l_id_329 % (2 + v_TP_293)))) % (4094 / v_TP_294))))], v__331[(((v_TP_294 + v_l_id_330) % v_TP_294) + (4096 * v_TP_293 * v_wg_id_327) + (4096 * (((4094 / v_TP_294) * ((1 + v_l_id_329) % (2 + v_TP_293))) / (4094 / v_TP_294))) + (4096 * (v_wg_id_328 / (4094 / v_TP_294))) + (v_TP_294 * ((v_wg_id_328 + ((4094 / v_TP_294) * ((1 + v_l_id_329) % (2 + v_TP_293)))) % (4094 / v_TP_294))))], v__331[(1 + ((v_TP_294 + v_l_id_330) % v_TP_294) + (4096 * v_TP_293 * v_wg_id_327) + (4096 * (((4094 / v_TP_294) * ((1 + v_l_id_329) % (2 + v_TP_293))) / (4094 / v_TP_294))) + (4096 * (v_wg_id_328 / (4094 / v_TP_294))) + (v_TP_294 * ((v_wg_id_328 + ((4094 / v_TP_294) * ((1 + v_l_id_329) % (2 + v_TP_293)))) % (4094 / v_TP_294))))], v__331[(2 + ((v_TP_294 + v_l_id_330) % v_TP_294) + (4096 * v_TP_293 * v_wg_id_327) + (4096 * (((4094 / v_TP_294) * ((1 + v_l_id_329) % (2 + v_TP_293))) / (4094 / v_TP_294))) + (4096 * (v_wg_id_328 / (4094 / v_TP_294))) + (v_TP_294 * ((v_wg_id_328 + ((4094 / v_TP_294) * ((1 + v_l_id_329) % (2 + v_TP_293)))) % (4094 / v_TP_294))))], v__331[(((v_l_id_330 + (2 * v_TP_294)) % v_TP_294) + (4096 * v_TP_293 * v_wg_id_327) + (4096 * (((4094 / v_TP_294) * ((2 + v_l_id_329) % (2 + v_TP_293))) / (4094 / v_TP_294))) + (4096 * (v_wg_id_328 / (4094 / v_TP_294))) + (v_TP_294 * ((v_wg_id_328 + ((4094 / v_TP_294) * ((2 + v_l_id_329) % (2 + v_TP_293)))) % (4094 / v_TP_294))))], v__331[(1 + ((v_l_id_330 + (2 * v_TP_294)) % v_TP_294) + (4096 * v_TP_293 * v_wg_id_327) + (4096 * (((4094 / v_TP_294) * ((2 + v_l_id_329) % (2 + v_TP_293))) / (4094 / v_TP_294))) + (4096 * (v_wg_id_328 / (4094 / v_TP_294))) + (v_TP_294 * ((v_wg_id_328 + ((4094 / v_TP_294) * ((2 + v_l_id_329) % (2 + v_TP_293)))) % (4094 / v_TP_294))))], v__331[(2 + ((v_l_id_330 + (2 * v_TP_294)) % v_TP_294) + (4096 * v_TP_293 * v_wg_id_327) + (4096 * (((4094 / v_TP_294) * ((2 + v_l_id_329) % (2 + v_TP_293))) / (4094 / v_TP_294))) + (4096 * (v_wg_id_328 / (4094 / v_TP_294))) + (v_TP_294 * ((v_wg_id_328 + ((4094 / v_TP_294) * ((2 + v_l_id_329) % (2 + v_TP_293)))) % (4094 / v_TP_294))))]); 
          v__335[(4097 + v_l_id_330 + (-4096 * ((v_l_id_330 + (v_TP_294 * ((v_wg_id_328 + ((4094 / v_TP_294) * ((v_l_id_329 + (v_TP_293 * v_wg_id_328)) % v_TP_293))) % (4094 / v_TP_294)))) / 4095)) + (4096 * v_TP_293 * v_wg_id_327) + (-16777216 * (((((4094 / v_TP_294) * ((v_l_id_329 + (v_TP_293 * v_wg_id_328)) % v_TP_293)) / (4094 / v_TP_294)) + (v_wg_id_328 / (4094 / v_TP_294)) + (v_TP_293 * v_wg_id_327)) / 4095)) + (4096 * (((4094 / v_TP_294) * ((v_l_id_329 + (v_TP_293 * v_wg_id_328)) % v_TP_293)) / (4094 / v_TP_294))) + (4096 * (v_wg_id_328 / (4094 / v_TP_294))) + (v_TP_294 * ((v_wg_id_328 + ((4094 / v_TP_294) * ((v_l_id_329 + (v_TP_293 * v_wg_id_328)) % v_TP_293))) % (4094 / v_TP_294))))] = id(v__334_0); 
        }
      }
    }
  }
}}


