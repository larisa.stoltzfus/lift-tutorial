
// High-level hash: d9610667c34db21b3b85dc5bdf67a09b199cf48e2fe915ba50f720758fe1c5d7
// Low-level hash: 04a8bf94e134c0c1eb58e95154d530a4ba25ca6f053ae43d3336997836bd1f5a

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_92" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_91" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__121, global float* v__125){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__124_0;
  
  for (int v_gl_id_117 = get_global_id(1); v_gl_id_117 < (4094 / v_TP_91); v_gl_id_117 = (v_gl_id_117 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_118 = get_global_id(0); v_gl_id_118 < (4094 / v_TP_92); v_gl_id_118 = (v_gl_id_118 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_119 = 0; v_i_119 < v_TP_91; v_i_119 = (1 + v_i_119)) {
        /* map_seq */
        for (int v_i_120 = 0; v_i_120 < v_TP_92; v_i_120 = (1 + v_i_120)) {
          v__124_0 = jacobi(v__121[(1 + (v_i_120 % v_TP_92) + (4096 * v_TP_91 * v_gl_id_117) + (4096 * (((4094 / v_TP_92) * (v_i_119 % (2 + v_TP_91))) / (4094 / v_TP_92))) + (4096 * (v_gl_id_118 / (4094 / v_TP_92))) + (v_TP_92 * ((v_gl_id_118 + ((4094 / v_TP_92) * (v_i_119 % (2 + v_TP_91)))) % (4094 / v_TP_92))))], v__121[(1 + ((v_i_120 + (2 * v_TP_92)) % v_TP_92) + (4096 * v_TP_91 * v_gl_id_117) + (4096 * (v_gl_id_118 / (4094 / v_TP_92))) + (4096 * (((4094 / v_TP_92) * ((2 + v_i_119) % (2 + v_TP_91))) / (4094 / v_TP_92))) + (v_TP_92 * ((v_gl_id_118 + ((4094 / v_TP_92) * ((2 + v_i_119) % (2 + v_TP_91)))) % (4094 / v_TP_92))))], v__121[(((v_TP_92 + v_i_120) % v_TP_92) + (4096 * v_TP_91 * v_gl_id_117) + (4096 * (v_gl_id_118 / (4094 / v_TP_92))) + (4096 * (((4094 / v_TP_92) * ((1 + v_i_119) % (2 + v_TP_91))) / (4094 / v_TP_92))) + (v_TP_92 * ((v_gl_id_118 + ((4094 / v_TP_92) * ((1 + v_i_119) % (2 + v_TP_91)))) % (4094 / v_TP_92))))], v__121[(2 + ((v_TP_92 + v_i_120) % v_TP_92) + (4096 * v_TP_91 * v_gl_id_117) + (4096 * (v_gl_id_118 / (4094 / v_TP_92))) + (4096 * (((4094 / v_TP_92) * ((1 + v_i_119) % (2 + v_TP_91))) / (4094 / v_TP_92))) + (v_TP_92 * ((v_gl_id_118 + ((4094 / v_TP_92) * ((1 + v_i_119) % (2 + v_TP_91)))) % (4094 / v_TP_92))))], v__121[(1 + ((v_TP_92 + v_i_120) % v_TP_92) + (4096 * v_TP_91 * v_gl_id_117) + (4096 * (v_gl_id_118 / (4094 / v_TP_92))) + (4096 * (((4094 / v_TP_92) * ((1 + v_i_119) % (2 + v_TP_91))) / (4094 / v_TP_92))) + (v_TP_92 * ((v_gl_id_118 + ((4094 / v_TP_92) * ((1 + v_i_119) % (2 + v_TP_91)))) % (4094 / v_TP_92))))]); 
          v__125[(4097 + v_i_120 + (4096 * v_TP_91 * v_gl_id_117) + (-16777216 * (((v_gl_id_118 / (4094 / v_TP_92)) + (((4094 / v_TP_92) * ((v_i_119 + (v_TP_91 * v_gl_id_118)) % v_TP_91)) / (4094 / v_TP_92)) + (v_TP_91 * v_gl_id_117)) / 4095)) + (-4096 * ((v_i_120 + (v_TP_92 * ((v_gl_id_118 + ((4094 / v_TP_92) * ((v_i_119 + (v_TP_91 * v_gl_id_118)) % v_TP_91))) % (4094 / v_TP_92)))) / 4095)) + (4096 * (v_gl_id_118 / (4094 / v_TP_92))) + (4096 * (((4094 / v_TP_92) * ((v_i_119 + (v_TP_91 * v_gl_id_118)) % v_TP_91)) / (4094 / v_TP_92))) + (v_TP_92 * ((v_gl_id_118 + ((4094 / v_TP_92) * ((v_i_119 + (v_TP_91 * v_gl_id_118)) % v_TP_91))) % (4094 / v_TP_92))))] = id(v__124_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


