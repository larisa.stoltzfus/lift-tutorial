
// High-level hash: f62cb1a6a4509dd2009c2f06b29816ce49d9f7184b15079f6abe9de74b4855b8
// Low-level hash: 8f702eeaf65eb4007fdea250a45cbc19678f09db4507eb9e0fd1bc5fe314df8b

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_128" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_127" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__182, global float* v__194){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__190[(4+(2*v_TP_127)+(2*v_TP_128)+(v_TP_127*v_TP_128))];
  /* Typed Value memory */
  /* Private Memory */
  float v__193_0;
  
  for (int v_wg_id_176 = get_group_id(1); v_wg_id_176 < (4094 / v_TP_127); v_wg_id_176 = (v_wg_id_176 + NUM_GROUPS_1)) {
    for (int v_wg_id_177 = get_group_id(0); v_wg_id_177 < (4094 / v_TP_128); v_wg_id_177 = (v_wg_id_177 + NUM_GROUPS_0)) {
      for (int v_l_id_178 = get_local_id(1); v_l_id_178 < (2 + v_TP_127); v_l_id_178 = (v_l_id_178 + LOCAL_SIZE_1)) {
        for (int v_l_id_179 = get_local_id(0); v_l_id_179 < (2 + v_TP_128); v_l_id_179 = (v_l_id_179 + LOCAL_SIZE_0)) {
          v__190[(v_l_id_179 + (2 * v_l_id_178) + (v_TP_128 * v_l_id_178))] = idfloat(v__182[(v_l_id_179 + (4096 * v_TP_127 * v_wg_id_176) + (4096 * (v_wg_id_177 / (4094 / v_TP_128))) + (4096 * (((4094 / v_TP_128) * (v_l_id_178 % (2 + v_TP_127))) / (4094 / v_TP_128))) + (v_TP_128 * ((v_wg_id_177 + ((4094 / v_TP_128) * (v_l_id_178 % (2 + v_TP_127)))) % (4094 / v_TP_128))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_180 = get_local_id(1); v_l_id_180 < v_TP_127; v_l_id_180 = (v_l_id_180 + LOCAL_SIZE_1)) {
        for (int v_l_id_181 = get_local_id(0); v_l_id_181 < v_TP_128; v_l_id_181 = (v_l_id_181 + LOCAL_SIZE_0)) {
          v__193_0 = jacobi(v__190[(1 + (v_l_id_181 % v_TP_128) + (2 * v_l_id_180) + (v_TP_128 * v_l_id_180))], v__190[(5 + v_l_id_181 + (2 * v_TP_128) + (2 * v_l_id_180) + (v_TP_128 * v_l_id_180))], v__190[(2 + v_TP_128 + ((v_TP_128 + v_l_id_181) % v_TP_128) + (2 * v_l_id_180) + (v_TP_128 * v_l_id_180))], v__190[(4 + v_TP_128 + ((v_TP_128 + v_l_id_181) % v_TP_128) + (2 * v_l_id_180) + (v_TP_128 * v_l_id_180))], v__190[(3 + v_TP_128 + ((v_TP_128 + v_l_id_181) % v_TP_128) + (2 * v_l_id_180) + (v_TP_128 * v_l_id_180))]); 
          v__194[(4097 + v_l_id_181 + (-4096 * ((v_l_id_181 + (v_TP_128 * ((v_wg_id_177 + ((4094 / v_TP_128) * ((v_l_id_180 + (v_TP_127 * v_wg_id_177)) % v_TP_127))) % (4094 / v_TP_128)))) / 4095)) + (4096 * v_TP_127 * v_wg_id_176) + (-16777216 * (((((4094 / v_TP_128) * ((v_l_id_180 + (v_TP_127 * v_wg_id_177)) % v_TP_127)) / (4094 / v_TP_128)) + (v_wg_id_177 / (4094 / v_TP_128)) + (v_TP_127 * v_wg_id_176)) / 4095)) + (4096 * (((4094 / v_TP_128) * ((v_l_id_180 + (v_TP_127 * v_wg_id_177)) % v_TP_127)) / (4094 / v_TP_128))) + (4096 * (v_wg_id_177 / (4094 / v_TP_128))) + (v_TP_128 * ((v_wg_id_177 + ((4094 / v_TP_128) * ((v_l_id_180 + (v_TP_127 * v_wg_id_177)) % v_TP_127))) % (4094 / v_TP_128))))] = id(v__193_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


