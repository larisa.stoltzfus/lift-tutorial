// High-level hash: f62cb1a6a4509dd2009c2f06b29816ce49d9f7184b15079f6abe9de74b4855b8
// Low-level hash: 04a8bf94e134c0c1eb58e95154d530a4ba25ca6f053ae43d3336997836bd1f5a
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__102, global float* v__105){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__104_0;
  
  for (int v_gl_id_98 = get_global_id(1); v_gl_id_98 < (4094 / 2); v_gl_id_98 = (v_gl_id_98 + 2048)) {
    for (int v_gl_id_99 = get_global_id(0); v_gl_id_99 < (4094 / 2); v_gl_id_99 = (v_gl_id_99 + 2048)) {
      /* map_seq */
      for (int v_i_100 = 0; v_i_100 < 2; v_i_100 = (1 + v_i_100)) {
        /* map_seq */
        for (int v_i_101 = 0; v_i_101 < 2; v_i_101 = (1 + v_i_101)) {
          v__104_0 = jacobi(v__102[(1 + (v_i_101 % 2) + (4096 * 2 * v_gl_id_98) + (4096 * (((4094 / 2) * (v_i_100 % (2 + 2))) / (4094 / 2))) + (4096 * (v_gl_id_99 / (4094 / 2))) + (2 * ((v_gl_id_99 + ((4094 / 2) * (v_i_100 % (2 + 2)))) % (4094 / 2))))], v__102[(1 + ((v_i_101 + (2 * 2)) % 2) + (4096 * 2 * v_gl_id_98) + (4096 * (v_gl_id_99 / (4094 / 2))) + (4096 * (((4094 / 2) * ((2 + v_i_100) % (2 + 2))) / (4094 / 2))) + (2 * ((v_gl_id_99 + ((4094 / 2) * ((2 + v_i_100) % (2 + 2)))) % (4094 / 2))))], v__102[(((2 + v_i_101) % 2) + (4096 * 2 * v_gl_id_98) + (4096 * (v_gl_id_99 / (4094 / 2))) + (4096 * (((4094 / 2) * ((1 + v_i_100) % (2 + 2))) / (4094 / 2))) + (2 * ((v_gl_id_99 + ((4094 / 2) * ((1 + v_i_100) % (2 + 2)))) % (4094 / 2))))], v__102[(2 + ((2 + v_i_101) % 2) + (4096 * 2 * v_gl_id_98) + (4096 * (v_gl_id_99 / (4094 / 2))) + (4096 * (((4094 / 2) * ((1 + v_i_100) % (2 + 2))) / (4094 / 2))) + (2 * ((v_gl_id_99 + ((4094 / 2) * ((1 + v_i_100) % (2 + 2)))) % (4094 / 2))))], v__102[(1 + ((2 + v_i_101) % 2) + (4096 * 2 * v_gl_id_98) + (4096 * (v_gl_id_99 / (4094 / 2))) + (4096 * (((4094 / 2) * ((1 + v_i_100) % (2 + 2))) / (4094 / 2))) + (2 * ((v_gl_id_99 + ((4094 / 2) * ((1 + v_i_100) % (2 + 2)))) % (4094 / 2))))]); 
          v__105[(4097 + v_i_101 + (4096 * 2 * v_gl_id_98) + (-16777216 * (((v_gl_id_99 / (4094 / 2)) + (((4094 / 2) * ((v_i_100 + (2 * v_gl_id_99)) % 2)) / (4094 / 2)) + (2 * v_gl_id_98)) / 4095)) + (-4096 * ((v_i_101 + (2 * ((v_gl_id_99 + ((4094 / 2) * ((v_i_100 + (2 * v_gl_id_99)) % 2))) % (4094 / 2)))) / 4095)) + (4096 * (v_gl_id_99 / (4094 / 2))) + (4096 * (((4094 / 2) * ((v_i_100 + (2 * v_gl_id_99)) % 2)) / (4094 / 2))) + (2 * ((v_gl_id_99 + ((4094 / 2) * ((v_i_100 + (2 * v_gl_id_99)) % 2))) % (4094 / 2))))] = id(v__104_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}
