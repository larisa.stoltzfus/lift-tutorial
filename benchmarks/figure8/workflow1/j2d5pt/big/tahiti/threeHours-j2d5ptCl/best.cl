// High-level hash: f9049a322a4dc19463e0c311028623c043b372f768b50e9f9d7f1c9f0158d8b3
// Low-level hash: 13bd7157213399b3e8f3a56f93303df8593ddf93be70fa9d3b826af43c65ec45
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__14, global float* v__17){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__16; 
  for (int v_gl_id_12 = get_global_id(1); v_gl_id_12 < 8190; v_gl_id_12 = (v_gl_id_12 + 8192)) {
    for (int v_gl_id_13 = get_global_id(0); v_gl_id_13 < 8190; v_gl_id_13 = (v_gl_id_13 + 4096)) {
      v__16 = jacobi(v__14[(1 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16385 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8192 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8194 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8193 + v_gl_id_13 + (8192 * v_gl_id_12))]); 
      v__17[(8193 + v_gl_id_13 + (8192 * v_gl_id_12))] = id(v__16); 
    }
  }
}}
