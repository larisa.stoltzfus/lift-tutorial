
// High-level hash: ad6f020667d982b1a7373026abbcb0fefe63ee24c3bbe7ed3324fcbef02cd46f
// Low-level hash: 1e668e77b840aa9fa6c98c56a7c2b50c804b04b87dd797cd457ebb1bae95dc73

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_160" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_159" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__236, global float* v__275){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__271[(4+(2*v_TP_159)+(2*v_TP_160)+(v_TP_159*v_TP_160))];
  /* Typed Value memory */
  /* Private Memory */
  float v__274_0;
  
  for (int v_wg_id_229 = get_group_id(1); v_wg_id_229 < (8190 / v_TP_159); v_wg_id_229 = (v_wg_id_229 + NUM_GROUPS_1)) {
    for (int v_wg_id_230 = get_group_id(0); v_wg_id_230 < (8190 / v_TP_160); v_wg_id_230 = (v_wg_id_230 + NUM_GROUPS_0)) {
      for (int v_l_id_232 = get_local_id(1); v_l_id_232 < (2 + v_TP_159); v_l_id_232 = (v_l_id_232 + LOCAL_SIZE_1)) {
        for (int v_l_id_233 = get_local_id(0); v_l_id_233 < (2 + v_TP_160); v_l_id_233 = (v_l_id_233 + LOCAL_SIZE_0)) {
          v__271[(v_l_id_233 + (2 * v_l_id_232) + (v_TP_160 * v_l_id_232))] = idfloat(v__236[(v_l_id_233 + (8192 * v_TP_159 * v_wg_id_229) + (8192 * (v_wg_id_230 / (8190 / v_TP_160))) + (8192 * (((8190 / v_TP_160) * (v_l_id_232 % (2 + v_TP_159))) / (8190 / v_TP_160))) + (v_TP_160 * ((v_wg_id_230 + ((8190 / v_TP_160) * (v_l_id_232 % (2 + v_TP_159)))) % (8190 / v_TP_160))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_234 = get_local_id(1); v_l_id_234 < v_TP_159; v_l_id_234 = (v_l_id_234 + LOCAL_SIZE_1)) {
        for (int v_l_id_235 = get_local_id(0); v_l_id_235 < v_TP_160; v_l_id_235 = (v_l_id_235 + LOCAL_SIZE_0)) {
          v__274_0 = jacobi(v__271[(1 + (v_l_id_235 % v_TP_160) + (2 * v_l_id_234) + (v_TP_160 * v_l_id_234))], v__271[(5 + v_l_id_235 + (2 * v_TP_160) + (2 * v_l_id_234) + (v_TP_160 * v_l_id_234))], v__271[(2 + v_TP_160 + ((v_TP_160 + v_l_id_235) % v_TP_160) + (2 * v_l_id_234) + (v_TP_160 * v_l_id_234))], v__271[(4 + v_TP_160 + ((v_TP_160 + v_l_id_235) % v_TP_160) + (2 * v_l_id_234) + (v_TP_160 * v_l_id_234))], v__271[(3 + v_TP_160 + ((v_TP_160 + v_l_id_235) % v_TP_160) + (2 * v_l_id_234) + (v_TP_160 * v_l_id_234))]); 
          v__275[(8193 + v_l_id_235 + (-8192 * ((v_l_id_235 + (v_TP_160 * ((v_wg_id_230 + ((8190 / v_TP_160) * ((v_l_id_234 + (v_TP_159 * v_wg_id_230)) % v_TP_159))) % (8190 / v_TP_160)))) / 8191)) + (8192 * v_TP_159 * v_wg_id_229) + (-67108864 * (((((8190 / v_TP_160) * ((v_l_id_234 + (v_TP_159 * v_wg_id_230)) % v_TP_159)) / (8190 / v_TP_160)) + (v_wg_id_230 / (8190 / v_TP_160)) + (v_TP_159 * v_wg_id_229)) / 8191)) + (8192 * (((8190 / v_TP_160) * ((v_l_id_234 + (v_TP_159 * v_wg_id_230)) % v_TP_159)) / (8190 / v_TP_160))) + (8192 * (v_wg_id_230 / (8190 / v_TP_160))) + (v_TP_160 * ((v_wg_id_230 + ((8190 / v_TP_160) * ((v_l_id_234 + (v_TP_159 * v_wg_id_230)) % v_TP_159))) % (8190 / v_TP_160))))] = id(v__274_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


