
// High-level hash: ad6f020667d982b1a7373026abbcb0fefe63ee24c3bbe7ed3324fcbef02cd46f
// Low-level hash: e43d66b78426a5e911e0f818e2395cc993e10eab30f60e15c1de54332734377c

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_44" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_43" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__79, global float* v__85){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__84_0;
  
  for (int v_wg_id_73 = get_group_id(1); v_wg_id_73 < (8190 / v_TP_43); v_wg_id_73 = (v_wg_id_73 + NUM_GROUPS_1)) {
    for (int v_wg_id_74 = get_group_id(0); v_wg_id_74 < (8190 / v_TP_44); v_wg_id_74 = (v_wg_id_74 + NUM_GROUPS_0)) {
      for (int v_l_id_76 = get_local_id(1); v_l_id_76 < v_TP_43; v_l_id_76 = (v_l_id_76 + LOCAL_SIZE_1)) {
        for (int v_l_id_78 = get_local_id(0); v_l_id_78 < v_TP_44; v_l_id_78 = (v_l_id_78 + LOCAL_SIZE_0)) {
          v__84_0 = jacobi(v__79[(1 + (v_l_id_78 % v_TP_44) + (8192 * v_TP_43 * v_wg_id_73) + (8192 * (v_wg_id_74 / (8190 / v_TP_44))) + (8192 * (((8190 / v_TP_44) * (v_l_id_76 % (2 + v_TP_43))) / (8190 / v_TP_44))) + (v_TP_44 * ((v_wg_id_74 + ((8190 / v_TP_44) * (v_l_id_76 % (2 + v_TP_43)))) % (8190 / v_TP_44))))], v__79[(1 + ((v_l_id_78 + (2 * v_TP_44)) % v_TP_44) + (8192 * v_TP_43 * v_wg_id_73) + (8192 * (((8190 / v_TP_44) * ((2 + v_l_id_76) % (2 + v_TP_43))) / (8190 / v_TP_44))) + (8192 * (v_wg_id_74 / (8190 / v_TP_44))) + (v_TP_44 * ((v_wg_id_74 + ((8190 / v_TP_44) * ((2 + v_l_id_76) % (2 + v_TP_43)))) % (8190 / v_TP_44))))], v__79[(((v_TP_44 + v_l_id_78) % v_TP_44) + (8192 * v_TP_43 * v_wg_id_73) + (8192 * (((8190 / v_TP_44) * ((1 + v_l_id_76) % (2 + v_TP_43))) / (8190 / v_TP_44))) + (8192 * (v_wg_id_74 / (8190 / v_TP_44))) + (v_TP_44 * ((v_wg_id_74 + ((8190 / v_TP_44) * ((1 + v_l_id_76) % (2 + v_TP_43)))) % (8190 / v_TP_44))))], v__79[(2 + ((v_TP_44 + v_l_id_78) % v_TP_44) + (8192 * v_TP_43 * v_wg_id_73) + (8192 * (((8190 / v_TP_44) * ((1 + v_l_id_76) % (2 + v_TP_43))) / (8190 / v_TP_44))) + (8192 * (v_wg_id_74 / (8190 / v_TP_44))) + (v_TP_44 * ((v_wg_id_74 + ((8190 / v_TP_44) * ((1 + v_l_id_76) % (2 + v_TP_43)))) % (8190 / v_TP_44))))], v__79[(1 + ((v_TP_44 + v_l_id_78) % v_TP_44) + (8192 * v_TP_43 * v_wg_id_73) + (8192 * (((8190 / v_TP_44) * ((1 + v_l_id_76) % (2 + v_TP_43))) / (8190 / v_TP_44))) + (8192 * (v_wg_id_74 / (8190 / v_TP_44))) + (v_TP_44 * ((v_wg_id_74 + ((8190 / v_TP_44) * ((1 + v_l_id_76) % (2 + v_TP_43)))) % (8190 / v_TP_44))))]); 
          v__85[(8193 + v_l_id_78 + (-8192 * ((v_l_id_78 + (v_TP_44 * ((v_wg_id_74 + ((8190 / v_TP_44) * ((v_l_id_76 + (v_TP_43 * v_wg_id_74)) % v_TP_43))) % (8190 / v_TP_44)))) / 8191)) + (8192 * v_TP_43 * v_wg_id_73) + (-67108864 * (((((8190 / v_TP_44) * ((v_l_id_76 + (v_TP_43 * v_wg_id_74)) % v_TP_43)) / (8190 / v_TP_44)) + (v_wg_id_74 / (8190 / v_TP_44)) + (v_TP_43 * v_wg_id_73)) / 8191)) + (8192 * (((8190 / v_TP_44) * ((v_l_id_76 + (v_TP_43 * v_wg_id_74)) % v_TP_43)) / (8190 / v_TP_44))) + (8192 * (v_wg_id_74 / (8190 / v_TP_44))) + (v_TP_44 * ((v_wg_id_74 + ((8190 / v_TP_44) * ((v_l_id_76 + (v_TP_43 * v_wg_id_74)) % v_TP_43))) % (8190 / v_TP_44))))] = id(v__84_0); 
        }
      }
    }
  }
}}


