
// High-level hash: f9704caf3700c46ad5bdea5e9fef14b6f9786de25e24324ba28aad3718a4b5e3
// Low-level hash: 1e668e77b840aa9fa6c98c56a7c2b50c804b04b87dd797cd457ebb1bae95dc73

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_175" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_174" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__215, global float* v__247){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__243[(4+(2*v_TP_174)+(2*v_TP_175)+(v_TP_174*v_TP_175))];
  /* Typed Value memory */
  /* Private Memory */
  float v__246_0;
  
  for (int v_wg_id_208 = get_group_id(1); v_wg_id_208 < (8190 / v_TP_174); v_wg_id_208 = (v_wg_id_208 + NUM_GROUPS_1)) {
    for (int v_wg_id_209 = get_group_id(0); v_wg_id_209 < (8190 / v_TP_175); v_wg_id_209 = (v_wg_id_209 + NUM_GROUPS_0)) {
      for (int v_l_id_210 = get_local_id(1); v_l_id_210 < (2 + v_TP_174); v_l_id_210 = (v_l_id_210 + LOCAL_SIZE_1)) {
        for (int v_l_id_211 = get_local_id(0); v_l_id_211 < (2 + v_TP_175); v_l_id_211 = (v_l_id_211 + LOCAL_SIZE_0)) {
          v__243[(v_l_id_211 + (2 * v_l_id_210) + (v_TP_175 * v_l_id_210))] = idfloat(v__215[(v_l_id_211 + (8192 * v_TP_174 * v_wg_id_208) + (8192 * (v_wg_id_209 / (8190 / v_TP_175))) + (8192 * (((8190 / v_TP_175) * (v_l_id_210 % (2 + v_TP_174))) / (8190 / v_TP_175))) + (v_TP_175 * ((v_wg_id_209 + ((8190 / v_TP_175) * (v_l_id_210 % (2 + v_TP_174)))) % (8190 / v_TP_175))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_213 = get_local_id(1); v_l_id_213 < v_TP_174; v_l_id_213 = (v_l_id_213 + LOCAL_SIZE_1)) {
        for (int v_l_id_214 = get_local_id(0); v_l_id_214 < v_TP_175; v_l_id_214 = (v_l_id_214 + LOCAL_SIZE_0)) {
          v__246_0 = jacobi(v__243[(1 + (v_l_id_214 % v_TP_175) + (2 * v_l_id_213) + (v_TP_175 * v_l_id_213))], v__243[(5 + v_l_id_214 + (2 * v_TP_175) + (2 * v_l_id_213) + (v_TP_175 * v_l_id_213))], v__243[(2 + v_TP_175 + ((v_TP_175 + v_l_id_214) % v_TP_175) + (2 * v_l_id_213) + (v_TP_175 * v_l_id_213))], v__243[(4 + v_TP_175 + ((v_TP_175 + v_l_id_214) % v_TP_175) + (2 * v_l_id_213) + (v_TP_175 * v_l_id_213))], v__243[(3 + v_TP_175 + ((v_TP_175 + v_l_id_214) % v_TP_175) + (2 * v_l_id_213) + (v_TP_175 * v_l_id_213))]); 
          v__247[(8193 + v_l_id_214 + (-8192 * ((v_l_id_214 + (v_TP_175 * ((v_wg_id_209 + ((8190 / v_TP_175) * ((v_l_id_213 + (v_TP_174 * v_wg_id_209)) % v_TP_174))) % (8190 / v_TP_175)))) / 8191)) + (8192 * v_TP_174 * v_wg_id_208) + (-67108864 * (((((8190 / v_TP_175) * ((v_l_id_213 + (v_TP_174 * v_wg_id_209)) % v_TP_174)) / (8190 / v_TP_175)) + (v_wg_id_209 / (8190 / v_TP_175)) + (v_TP_174 * v_wg_id_208)) / 8191)) + (8192 * (((8190 / v_TP_175) * ((v_l_id_213 + (v_TP_174 * v_wg_id_209)) % v_TP_174)) / (8190 / v_TP_175))) + (8192 * (v_wg_id_209 / (8190 / v_TP_175))) + (v_TP_175 * ((v_wg_id_209 + ((8190 / v_TP_175) * ((v_l_id_213 + (v_TP_174 * v_wg_id_209)) % v_TP_174))) % (8190 / v_TP_175))))] = id(v__246_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


