// High-level hash: f9704caf3700c46ad5bdea5e9fef14b6f9786de25e24324ba28aad3718a4b5e3
// Low-level hash: 56606dbe9003e1c33dc9235f5c029f72a167c147abae14580c8b8bf36b60f0ce
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__102, global float* v__105){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__104_0;
  
  for (int v_gl_id_98 = get_global_id(1); v_gl_id_98 < (8190 / 2); v_gl_id_98 = (v_gl_id_98 + 4096)) {
    for (int v_gl_id_99 = get_global_id(0); v_gl_id_99 < (8190 / 2); v_gl_id_99 = (v_gl_id_99 + 4096)) {
      /* map_seq */
      for (int v_i_100 = 0; v_i_100 < 2; v_i_100 = (1 + v_i_100)) {
        /* map_seq */
        for (int v_i_101 = 0; v_i_101 < 2; v_i_101 = (1 + v_i_101)) {
          v__104_0 = jacobi(v__102[(1 + (v_i_101 % 2) + (8192 * 2 * v_gl_id_98) + (8192 * (((8190 / 2) * (v_i_100 % (2 + 2))) / (8190 / 2))) + (8192 * (v_gl_id_99 / (8190 / 2))) + (2 * ((v_gl_id_99 + ((8190 / 2) * (v_i_100 % (2 + 2)))) % (8190 / 2))))], v__102[(1 + ((v_i_101 + (2 * 2)) % 2) + (8192 * 2 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8190 / 2))) + (8192 * (((8190 / 2) * ((2 + v_i_100) % (2 + 2))) / (8190 / 2))) + (2 * ((v_gl_id_99 + ((8190 / 2) * ((2 + v_i_100) % (2 + 2)))) % (8190 / 2))))], v__102[(((2 + v_i_101) % 2) + (8192 * 2 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8190 / 2))) + (8192 * (((8190 / 2) * ((1 + v_i_100) % (2 + 2))) / (8190 / 2))) + (2 * ((v_gl_id_99 + ((8190 / 2) * ((1 + v_i_100) % (2 + 2)))) % (8190 / 2))))], v__102[(2 + ((2 + v_i_101) % 2) + (8192 * 2 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8190 / 2))) + (8192 * (((8190 / 2) * ((1 + v_i_100) % (2 + 2))) / (8190 / 2))) + (2 * ((v_gl_id_99 + ((8190 / 2) * ((1 + v_i_100) % (2 + 2)))) % (8190 / 2))))], v__102[(1 + ((2 + v_i_101) % 2) + (8192 * 2 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8190 / 2))) + (8192 * (((8190 / 2) * ((1 + v_i_100) % (2 + 2))) / (8190 / 2))) + (2 * ((v_gl_id_99 + ((8190 / 2) * ((1 + v_i_100) % (2 + 2)))) % (8190 / 2))))]); 
          v__105[(8193 + v_i_101 + (8192 * 2 * v_gl_id_98) + (-67108864 * (((v_gl_id_99 / (8190 / 2)) + (((8190 / 2) * ((v_i_100 + (2 * v_gl_id_99)) % 2)) / (8190 / 2)) + (2 * v_gl_id_98)) / 8191)) + (-8192 * ((v_i_101 + (2 * ((v_gl_id_99 + ((8190 / 2) * ((v_i_100 + (2 * v_gl_id_99)) % 2))) % (8190 / 2)))) / 8191)) + (8192 * (v_gl_id_99 / (8190 / 2))) + (8192 * (((8190 / 2) * ((v_i_100 + (2 * v_gl_id_99)) % 2)) / (8190 / 2))) + (2 * ((v_gl_id_99 + ((8190 / 2) * ((v_i_100 + (2 * v_gl_id_99)) % 2))) % (8190 / 2))))] = id(v__104_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}
