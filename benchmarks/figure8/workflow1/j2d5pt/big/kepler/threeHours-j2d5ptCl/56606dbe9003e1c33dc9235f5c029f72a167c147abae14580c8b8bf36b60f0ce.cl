
// High-level hash: f9704caf3700c46ad5bdea5e9fef14b6f9786de25e24324ba28aad3718a4b5e3
// Low-level hash: 56606dbe9003e1c33dc9235f5c029f72a167c147abae14580c8b8bf36b60f0ce

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_77" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_76" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__102, global float* v__105){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__104_0;
  
  for (int v_gl_id_98 = get_global_id(1); v_gl_id_98 < (8190 / v_TP_76); v_gl_id_98 = (v_gl_id_98 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_99 = get_global_id(0); v_gl_id_99 < (8190 / v_TP_77); v_gl_id_99 = (v_gl_id_99 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_100 = 0; v_i_100 < v_TP_76; v_i_100 = (1 + v_i_100)) {
        /* map_seq */
        for (int v_i_101 = 0; v_i_101 < v_TP_77; v_i_101 = (1 + v_i_101)) {
          v__104_0 = jacobi(v__102[(1 + (v_i_101 % v_TP_77) + (8192 * v_TP_76 * v_gl_id_98) + (8192 * (((8190 / v_TP_77) * (v_i_100 % (2 + v_TP_76))) / (8190 / v_TP_77))) + (8192 * (v_gl_id_99 / (8190 / v_TP_77))) + (v_TP_77 * ((v_gl_id_99 + ((8190 / v_TP_77) * (v_i_100 % (2 + v_TP_76)))) % (8190 / v_TP_77))))], v__102[(1 + ((v_i_101 + (2 * v_TP_77)) % v_TP_77) + (8192 * v_TP_76 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8190 / v_TP_77))) + (8192 * (((8190 / v_TP_77) * ((2 + v_i_100) % (2 + v_TP_76))) / (8190 / v_TP_77))) + (v_TP_77 * ((v_gl_id_99 + ((8190 / v_TP_77) * ((2 + v_i_100) % (2 + v_TP_76)))) % (8190 / v_TP_77))))], v__102[(((v_TP_77 + v_i_101) % v_TP_77) + (8192 * v_TP_76 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8190 / v_TP_77))) + (8192 * (((8190 / v_TP_77) * ((1 + v_i_100) % (2 + v_TP_76))) / (8190 / v_TP_77))) + (v_TP_77 * ((v_gl_id_99 + ((8190 / v_TP_77) * ((1 + v_i_100) % (2 + v_TP_76)))) % (8190 / v_TP_77))))], v__102[(2 + ((v_TP_77 + v_i_101) % v_TP_77) + (8192 * v_TP_76 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8190 / v_TP_77))) + (8192 * (((8190 / v_TP_77) * ((1 + v_i_100) % (2 + v_TP_76))) / (8190 / v_TP_77))) + (v_TP_77 * ((v_gl_id_99 + ((8190 / v_TP_77) * ((1 + v_i_100) % (2 + v_TP_76)))) % (8190 / v_TP_77))))], v__102[(1 + ((v_TP_77 + v_i_101) % v_TP_77) + (8192 * v_TP_76 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8190 / v_TP_77))) + (8192 * (((8190 / v_TP_77) * ((1 + v_i_100) % (2 + v_TP_76))) / (8190 / v_TP_77))) + (v_TP_77 * ((v_gl_id_99 + ((8190 / v_TP_77) * ((1 + v_i_100) % (2 + v_TP_76)))) % (8190 / v_TP_77))))]); 
          v__105[(8193 + v_i_101 + (8192 * v_TP_76 * v_gl_id_98) + (-67108864 * (((v_gl_id_99 / (8190 / v_TP_77)) + (((8190 / v_TP_77) * ((v_i_100 + (v_TP_76 * v_gl_id_99)) % v_TP_76)) / (8190 / v_TP_77)) + (v_TP_76 * v_gl_id_98)) / 8191)) + (-8192 * ((v_i_101 + (v_TP_77 * ((v_gl_id_99 + ((8190 / v_TP_77) * ((v_i_100 + (v_TP_76 * v_gl_id_99)) % v_TP_76))) % (8190 / v_TP_77)))) / 8191)) + (8192 * (v_gl_id_99 / (8190 / v_TP_77))) + (8192 * (((8190 / v_TP_77) * ((v_i_100 + (v_TP_76 * v_gl_id_99)) % v_TP_76)) / (8190 / v_TP_77))) + (v_TP_77 * ((v_gl_id_99 + ((8190 / v_TP_77) * ((v_i_100 + (v_TP_76 * v_gl_id_99)) % v_TP_76))) % (8190 / v_TP_77))))] = id(v__104_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


