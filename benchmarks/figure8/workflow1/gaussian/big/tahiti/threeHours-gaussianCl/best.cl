// High-level hash: 1fb39cef1c7a8576bba4ac4763ea6cfece8da49f24d50d35e2036fe2aa785455
// Low-level hash: 87d266939edd240dccde5cff86cf5a5c77e2ec456b00e03a6097670736707603
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__14, global float* v__17){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__16; 
  for (int v_gl_id_12 = get_global_id(1); v_gl_id_12 < 8188; v_gl_id_12 = (v_gl_id_12 + 8192)) {
    for (int v_gl_id_13 = get_global_id(0); v_gl_id_13 < 8188; v_gl_id_13 = (v_gl_id_13 + 512)) {
      v__16 = jacobi(v__14[(v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(1 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(2 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(3 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(4 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8192 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8193 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8194 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8195 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8196 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16384 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16385 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16386 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16387 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16388 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(24576 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(24577 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(24578 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(24579 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(24580 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(32768 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(32769 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(32770 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(32771 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(32772 + v_gl_id_13 + (8192 * v_gl_id_12))]); 
      v__17[(16386 + v_gl_id_13 + (8192 * v_gl_id_12))] = id(v__16); 
    }
  }
}}
