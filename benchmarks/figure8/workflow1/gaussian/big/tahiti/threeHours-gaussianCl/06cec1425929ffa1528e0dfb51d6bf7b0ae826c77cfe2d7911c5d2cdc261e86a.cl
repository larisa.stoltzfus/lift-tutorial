
// High-level hash: dcc51678d87162f9202787a158fe83780cdccf4c3ddcf9f7384c8a0ae764019e
// Low-level hash: 06cec1425929ffa1528e0dfb51d6bf7b0ae826c77cfe2d7911c5d2cdc261e86a

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_42" \
 type "int" \
 range "atf::interval<int>(1,8188)" \
 constraint "atf::divides(8188)"
#atf::tp name "v_TP_41" \
 type "int" \
 range "atf::interval<int>(1,8188)" \
 constraint "atf::divides(8188)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__57, global float* v__60){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__59_0;
  
  for (int v_wg_id_53 = get_group_id(1); v_wg_id_53 < (8188 / v_TP_41); v_wg_id_53 = (v_wg_id_53 + NUM_GROUPS_1)) {
    for (int v_wg_id_54 = get_group_id(0); v_wg_id_54 < (8188 / v_TP_42); v_wg_id_54 = (v_wg_id_54 + NUM_GROUPS_0)) {
      for (int v_l_id_55 = get_local_id(1); v_l_id_55 < v_TP_41; v_l_id_55 = (v_l_id_55 + LOCAL_SIZE_1)) {
        for (int v_l_id_56 = get_local_id(0); v_l_id_56 < v_TP_42; v_l_id_56 = (v_l_id_56 + LOCAL_SIZE_0)) {
          v__59_0 = jacobi(v__57[((v_l_id_56 % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (8192 * (((8188 / v_TP_42) * (v_l_id_55 % (4 + v_TP_41))) / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * (v_l_id_55 % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(1 + (v_l_id_56 % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (8192 * (((8188 / v_TP_42) * (v_l_id_55 % (4 + v_TP_41))) / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * (v_l_id_55 % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(2 + (v_l_id_56 % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (8192 * (((8188 / v_TP_42) * (v_l_id_55 % (4 + v_TP_41))) / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * (v_l_id_55 % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(3 + (v_l_id_56 % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (8192 * (((8188 / v_TP_42) * (v_l_id_55 % (4 + v_TP_41))) / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * (v_l_id_55 % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(4 + (v_l_id_56 % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (8192 * (((8188 / v_TP_42) * (v_l_id_55 % (4 + v_TP_41))) / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * (v_l_id_55 % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(((v_TP_42 + v_l_id_56) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((1 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((1 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(1 + ((v_TP_42 + v_l_id_56) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((1 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((1 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(2 + ((v_TP_42 + v_l_id_56) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((1 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((1 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(3 + ((v_TP_42 + v_l_id_56) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((1 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((1 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(4 + ((v_TP_42 + v_l_id_56) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((1 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((1 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(((v_l_id_56 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((2 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((2 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(1 + ((v_l_id_56 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((2 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((2 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(2 + ((v_l_id_56 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((2 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((2 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(3 + ((v_l_id_56 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((2 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((2 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(4 + ((v_l_id_56 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((2 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((2 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(((v_l_id_56 + (3 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((3 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((3 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(1 + ((v_l_id_56 + (3 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((3 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((3 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(2 + ((v_l_id_56 + (3 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((3 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((3 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(3 + ((v_l_id_56 + (3 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((3 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((3 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(4 + ((v_l_id_56 + (3 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((3 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((3 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(((v_l_id_56 + (4 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((4 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((4 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(1 + ((v_l_id_56 + (4 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((4 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((4 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(2 + ((v_l_id_56 + (4 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((4 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((4 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(3 + ((v_l_id_56 + (4 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((4 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((4 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))], v__57[(4 + ((v_l_id_56 + (4 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_wg_id_53) + (8192 * (((8188 / v_TP_42) * ((4 + v_l_id_55) % (4 + v_TP_41))) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((4 + v_l_id_55) % (4 + v_TP_41)))) % (8188 / v_TP_42))))]); 
          v__60[(16386 + v_l_id_56 + (-8192 * ((v_l_id_56 + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((v_l_id_55 + (v_TP_41 * v_wg_id_54)) % v_TP_41))) % (8188 / v_TP_42)))) / 8191)) + (8192 * v_TP_41 * v_wg_id_53) + (-67108864 * (((((8188 / v_TP_42) * ((v_l_id_55 + (v_TP_41 * v_wg_id_54)) % v_TP_41)) / (8188 / v_TP_42)) + (v_wg_id_54 / (8188 / v_TP_42)) + (v_TP_41 * v_wg_id_53)) / 8191)) + (8192 * (((8188 / v_TP_42) * ((v_l_id_55 + (v_TP_41 * v_wg_id_54)) % v_TP_41)) / (8188 / v_TP_42))) + (8192 * (v_wg_id_54 / (8188 / v_TP_42))) + (v_TP_42 * ((v_wg_id_54 + ((8188 / v_TP_42) * ((v_l_id_55 + (v_TP_41 * v_wg_id_54)) % v_TP_41))) % (8188 / v_TP_42))))] = id(v__59_0); 
        }
      }
    }
  }
}}


