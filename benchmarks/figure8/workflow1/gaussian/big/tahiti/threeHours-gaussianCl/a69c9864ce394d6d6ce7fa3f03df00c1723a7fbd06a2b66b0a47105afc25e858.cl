
// High-level hash: dcc51678d87162f9202787a158fe83780cdccf4c3ddcf9f7384c8a0ae764019e
// Low-level hash: a69c9864ce394d6d6ce7fa3f03df00c1723a7fbd06a2b66b0a47105afc25e858

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_87" \
 type "int" \
 range "atf::interval<int>(1,8188)" \
 constraint "atf::divides(8188)"
#atf::tp name "v_TP_86" \
 type "int" \
 range "atf::interval<int>(1,8188)" \
 constraint "atf::divides(8188)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__102, global float* v__105){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__104_0;
  
  for (int v_gl_id_98 = get_global_id(1); v_gl_id_98 < (8188 / v_TP_86); v_gl_id_98 = (v_gl_id_98 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_99 = get_global_id(0); v_gl_id_99 < (8188 / v_TP_87); v_gl_id_99 = (v_gl_id_99 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_100 = 0; v_i_100 < v_TP_86; v_i_100 = (1 + v_i_100)) {
        /* map_seq */
        for (int v_i_101 = 0; v_i_101 < v_TP_87; v_i_101 = (1 + v_i_101)) {
          v__104_0 = jacobi(v__102[((v_i_101 % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (((8188 / v_TP_87) * (v_i_100 % (4 + v_TP_86))) / (8188 / v_TP_87))) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * (v_i_100 % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(1 + (v_i_101 % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (((8188 / v_TP_87) * (v_i_100 % (4 + v_TP_86))) / (8188 / v_TP_87))) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * (v_i_100 % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(2 + (v_i_101 % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (((8188 / v_TP_87) * (v_i_100 % (4 + v_TP_86))) / (8188 / v_TP_87))) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * (v_i_100 % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(3 + (v_i_101 % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (((8188 / v_TP_87) * (v_i_100 % (4 + v_TP_86))) / (8188 / v_TP_87))) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * (v_i_100 % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(4 + (v_i_101 % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (((8188 / v_TP_87) * (v_i_100 % (4 + v_TP_86))) / (8188 / v_TP_87))) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * (v_i_100 % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(((v_TP_87 + v_i_101) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((1 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((1 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(1 + ((v_TP_87 + v_i_101) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((1 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((1 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(2 + ((v_TP_87 + v_i_101) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((1 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((1 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(3 + ((v_TP_87 + v_i_101) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((1 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((1 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(4 + ((v_TP_87 + v_i_101) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((1 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((1 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(((v_i_101 + (2 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((2 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((2 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(1 + ((v_i_101 + (2 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((2 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((2 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(2 + ((v_i_101 + (2 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((2 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((2 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(3 + ((v_i_101 + (2 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((2 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((2 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(4 + ((v_i_101 + (2 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((2 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((2 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(((v_i_101 + (3 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((3 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((3 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(1 + ((v_i_101 + (3 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((3 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((3 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(2 + ((v_i_101 + (3 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((3 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((3 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(3 + ((v_i_101 + (3 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((3 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((3 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(4 + ((v_i_101 + (3 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((3 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((3 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(((v_i_101 + (4 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((4 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((4 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(1 + ((v_i_101 + (4 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((4 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((4 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(2 + ((v_i_101 + (4 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((4 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((4 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(3 + ((v_i_101 + (4 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((4 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((4 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))], v__102[(4 + ((v_i_101 + (4 * v_TP_87)) % v_TP_87) + (8192 * v_TP_86 * v_gl_id_98) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((4 + v_i_100) % (4 + v_TP_86))) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((4 + v_i_100) % (4 + v_TP_86)))) % (8188 / v_TP_87))))]); 
          v__105[(16386 + v_i_101 + (8192 * v_TP_86 * v_gl_id_98) + (-67108864 * (((v_gl_id_99 / (8188 / v_TP_87)) + (((8188 / v_TP_87) * ((v_i_100 + (v_TP_86 * v_gl_id_99)) % v_TP_86)) / (8188 / v_TP_87)) + (v_TP_86 * v_gl_id_98)) / 8191)) + (-8192 * ((v_i_101 + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((v_i_100 + (v_TP_86 * v_gl_id_99)) % v_TP_86))) % (8188 / v_TP_87)))) / 8191)) + (8192 * (v_gl_id_99 / (8188 / v_TP_87))) + (8192 * (((8188 / v_TP_87) * ((v_i_100 + (v_TP_86 * v_gl_id_99)) % v_TP_86)) / (8188 / v_TP_87))) + (v_TP_87 * ((v_gl_id_99 + ((8188 / v_TP_87) * ((v_i_100 + (v_TP_86 * v_gl_id_99)) % v_TP_86))) % (8188 / v_TP_87))))] = id(v__104_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


