
// High-level hash: 1fb39cef1c7a8576bba4ac4763ea6cfece8da49f24d50d35e2036fe2aa785455
// Low-level hash: 87d266939edd240dccde5cff86cf5a5c77e2ec456b00e03a6097670736707603

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__14, global float* v__17){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__16; 
  for (int v_gl_id_12 = get_global_id(1); v_gl_id_12 < 8188; v_gl_id_12 = (v_gl_id_12 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_13 = get_global_id(0); v_gl_id_13 < 8188; v_gl_id_13 = (v_gl_id_13 + GLOBAL_SIZE_0)) {
      v__16 = jacobi(v__14[(v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(1 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(2 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(3 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(4 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8192 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8193 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8194 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8195 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8196 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16384 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16385 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16386 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16387 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16388 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(24576 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(24577 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(24578 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(24579 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(24580 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(32768 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(32769 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(32770 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(32771 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(32772 + v_gl_id_13 + (8192 * v_gl_id_12))]); 
      v__17[(16386 + v_gl_id_13 + (8192 * v_gl_id_12))] = id(v__16); 
    }
  }
}}


