
// High-level hash: c720d531d84c51f53e6069501314edc331cd4463c76acd4222a33cce8c14745f
// Low-level hash: 8c9ae1dc92f17b764da04e58a3d8790c9817d091f9bd4743e770d79da0fc0aec

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_44" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"
#atf::tp name "v_TP_43" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__74, global float* v__80){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__79_0;
  
  for (int v_wg_id_64 = get_group_id(1); v_wg_id_64 < (4092 / v_TP_43); v_wg_id_64 = (v_wg_id_64 + NUM_GROUPS_1)) {
    for (int v_wg_id_71 = get_group_id(0); v_wg_id_71 < (4092 / v_TP_44); v_wg_id_71 = (v_wg_id_71 + NUM_GROUPS_0)) {
      for (int v_l_id_72 = get_local_id(1); v_l_id_72 < v_TP_43; v_l_id_72 = (v_l_id_72 + LOCAL_SIZE_1)) {
        for (int v_l_id_73 = get_local_id(0); v_l_id_73 < v_TP_44; v_l_id_73 = (v_l_id_73 + LOCAL_SIZE_0)) {
          v__79_0 = jacobi(v__74[((v_l_id_73 % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (4096 * (((4092 / v_TP_44) * (v_l_id_72 % (4 + v_TP_43))) / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * (v_l_id_72 % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(1 + (v_l_id_73 % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (4096 * (((4092 / v_TP_44) * (v_l_id_72 % (4 + v_TP_43))) / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * (v_l_id_72 % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(2 + (v_l_id_73 % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (4096 * (((4092 / v_TP_44) * (v_l_id_72 % (4 + v_TP_43))) / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * (v_l_id_72 % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(3 + (v_l_id_73 % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (4096 * (((4092 / v_TP_44) * (v_l_id_72 % (4 + v_TP_43))) / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * (v_l_id_72 % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(4 + (v_l_id_73 % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (4096 * (((4092 / v_TP_44) * (v_l_id_72 % (4 + v_TP_43))) / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * (v_l_id_72 % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(((v_TP_44 + v_l_id_73) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((1 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((1 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(1 + ((v_TP_44 + v_l_id_73) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((1 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((1 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(2 + ((v_TP_44 + v_l_id_73) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((1 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((1 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(3 + ((v_TP_44 + v_l_id_73) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((1 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((1 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(4 + ((v_TP_44 + v_l_id_73) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((1 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((1 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(((v_l_id_73 + (2 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((2 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((2 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(1 + ((v_l_id_73 + (2 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((2 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((2 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(2 + ((v_l_id_73 + (2 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((2 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((2 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(3 + ((v_l_id_73 + (2 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((2 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((2 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(4 + ((v_l_id_73 + (2 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((2 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((2 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(((v_l_id_73 + (3 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((3 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((3 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(1 + ((v_l_id_73 + (3 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((3 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((3 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(2 + ((v_l_id_73 + (3 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((3 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((3 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(3 + ((v_l_id_73 + (3 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((3 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((3 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(4 + ((v_l_id_73 + (3 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((3 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((3 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(((v_l_id_73 + (4 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((4 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((4 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(1 + ((v_l_id_73 + (4 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((4 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((4 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(2 + ((v_l_id_73 + (4 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((4 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((4 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(3 + ((v_l_id_73 + (4 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((4 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((4 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))], v__74[(4 + ((v_l_id_73 + (4 * v_TP_44)) % v_TP_44) + (4096 * v_TP_43 * v_wg_id_64) + (4096 * (((4092 / v_TP_44) * ((4 + v_l_id_72) % (4 + v_TP_43))) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((4 + v_l_id_72) % (4 + v_TP_43)))) % (4092 / v_TP_44))))]); 
          v__80[(8194 + v_l_id_73 + (-4096 * ((v_l_id_73 + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((v_l_id_72 + (v_TP_43 * v_wg_id_71)) % v_TP_43))) % (4092 / v_TP_44)))) / 4095)) + (4096 * v_TP_43 * v_wg_id_64) + (-16777216 * (((((4092 / v_TP_44) * ((v_l_id_72 + (v_TP_43 * v_wg_id_71)) % v_TP_43)) / (4092 / v_TP_44)) + (v_wg_id_71 / (4092 / v_TP_44)) + (v_TP_43 * v_wg_id_64)) / 4095)) + (4096 * (((4092 / v_TP_44) * ((v_l_id_72 + (v_TP_43 * v_wg_id_71)) % v_TP_43)) / (4092 / v_TP_44))) + (4096 * (v_wg_id_71 / (4092 / v_TP_44))) + (v_TP_44 * ((v_wg_id_71 + ((4092 / v_TP_44) * ((v_l_id_72 + (v_TP_43 * v_wg_id_71)) % v_TP_43))) % (4092 / v_TP_44))))] = id(v__79_0); 
        }
      }
    }
  }
}}


