
// High-level hash: c720d531d84c51f53e6069501314edc331cd4463c76acd4222a33cce8c14745f
// Low-level hash: e9bb366e5bb97d4e79817113e46f2e8de13740f17ec1e03065d1b016c49ad80f

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_65" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"
#atf::tp name "v_TP_64" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__80, global float* v__83){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__82_0;
  
  for (int v_gl_id_76 = get_global_id(0); v_gl_id_76 < (4092 / v_TP_64); v_gl_id_76 = (v_gl_id_76 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_77 = get_global_id(1); v_gl_id_77 < (4092 / v_TP_65); v_gl_id_77 = (v_gl_id_77 + GLOBAL_SIZE_1)) {
      /* map_seq */
      for (int v_i_78 = 0; v_i_78 < v_TP_64; v_i_78 = (1 + v_i_78)) {
        /* map_seq */
        for (int v_i_79 = 0; v_i_79 < v_TP_65; v_i_79 = (1 + v_i_79)) {
          v__82_0 = jacobi(v__80[((v_i_79 % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (((4092 / v_TP_65) * (v_i_78 % (4 + v_TP_64))) / (4092 / v_TP_65))) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * (v_i_78 % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(1 + (v_i_79 % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (((4092 / v_TP_65) * (v_i_78 % (4 + v_TP_64))) / (4092 / v_TP_65))) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * (v_i_78 % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(2 + (v_i_79 % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (((4092 / v_TP_65) * (v_i_78 % (4 + v_TP_64))) / (4092 / v_TP_65))) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * (v_i_78 % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(3 + (v_i_79 % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (((4092 / v_TP_65) * (v_i_78 % (4 + v_TP_64))) / (4092 / v_TP_65))) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * (v_i_78 % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(4 + (v_i_79 % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (((4092 / v_TP_65) * (v_i_78 % (4 + v_TP_64))) / (4092 / v_TP_65))) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * (v_i_78 % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(((v_TP_65 + v_i_79) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((1 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((1 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(1 + ((v_TP_65 + v_i_79) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((1 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((1 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(2 + ((v_TP_65 + v_i_79) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((1 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((1 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(3 + ((v_TP_65 + v_i_79) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((1 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((1 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(4 + ((v_TP_65 + v_i_79) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((1 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((1 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(((v_i_79 + (2 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((2 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((2 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(1 + ((v_i_79 + (2 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((2 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((2 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(2 + ((v_i_79 + (2 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((2 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((2 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(3 + ((v_i_79 + (2 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((2 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((2 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(4 + ((v_i_79 + (2 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((2 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((2 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(((v_i_79 + (3 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((3 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((3 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(1 + ((v_i_79 + (3 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((3 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((3 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(2 + ((v_i_79 + (3 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((3 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((3 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(3 + ((v_i_79 + (3 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((3 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((3 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(4 + ((v_i_79 + (3 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((3 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((3 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(((v_i_79 + (4 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((4 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((4 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(1 + ((v_i_79 + (4 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((4 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((4 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(2 + ((v_i_79 + (4 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((4 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((4 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(3 + ((v_i_79 + (4 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((4 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((4 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))], v__80[(4 + ((v_i_79 + (4 * v_TP_65)) % v_TP_65) + (4096 * v_TP_64 * v_gl_id_76) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((4 + v_i_78) % (4 + v_TP_64))) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((4 + v_i_78) % (4 + v_TP_64)))) % (4092 / v_TP_65))))]); 
          v__83[(8194 + v_i_79 + (4096 * v_TP_64 * v_gl_id_76) + (-16777216 * (((v_gl_id_77 / (4092 / v_TP_65)) + (((4092 / v_TP_65) * ((v_i_78 + (v_TP_64 * v_gl_id_77)) % v_TP_64)) / (4092 / v_TP_65)) + (v_TP_64 * v_gl_id_76)) / 4095)) + (-4096 * ((v_i_79 + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((v_i_78 + (v_TP_64 * v_gl_id_77)) % v_TP_64))) % (4092 / v_TP_65)))) / 4095)) + (4096 * (v_gl_id_77 / (4092 / v_TP_65))) + (4096 * (((4092 / v_TP_65) * ((v_i_78 + (v_TP_64 * v_gl_id_77)) % v_TP_64)) / (4092 / v_TP_65))) + (v_TP_65 * ((v_gl_id_77 + ((4092 / v_TP_65) * ((v_i_78 + (v_TP_64 * v_gl_id_77)) % v_TP_64))) % (4092 / v_TP_65))))] = id(v__82_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


