
// High-level hash: c720d531d84c51f53e6069501314edc331cd4463c76acd4222a33cce8c14745f
// Low-level hash: d678f40d2ccda400fb1cb10be22ffadea56aa3114cde60982de87bd30f564f53

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_2104" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"
#atf::tp name "v_TP_2103" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__2125, global float* v__2132){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__2129[(16+(4*v_TP_2103)+(4*v_TP_2104)+(v_TP_2103*v_TP_2104))];
  /* Typed Value memory */
  /* Private Memory */
  float v__2131_0;
  
  for (int v_wg_id_2119 = get_group_id(0); v_wg_id_2119 < (4092 / v_TP_2103); v_wg_id_2119 = (v_wg_id_2119 + NUM_GROUPS_0)) {
    for (int v_wg_id_2120 = get_group_id(1); v_wg_id_2120 < (4092 / v_TP_2104); v_wg_id_2120 = (v_wg_id_2120 + NUM_GROUPS_1)) {
      for (int v_l_id_2121 = get_local_id(0); v_l_id_2121 < (4 + v_TP_2103); v_l_id_2121 = (v_l_id_2121 + LOCAL_SIZE_0)) {
        for (int v_l_id_2122 = get_local_id(1); v_l_id_2122 < (4 + v_TP_2104); v_l_id_2122 = (v_l_id_2122 + LOCAL_SIZE_1)) {
          v__2129[(v_l_id_2122 + (4 * v_l_id_2121) + (v_TP_2104 * v_l_id_2121))] = idfloat(v__2125[(v_l_id_2122 + (4096 * v_TP_2103 * v_wg_id_2119) + (4096 * (v_wg_id_2120 / (4092 / v_TP_2104))) + (4096 * (((4092 / v_TP_2104) * (v_l_id_2121 % (4 + v_TP_2103))) / (4092 / v_TP_2104))) + (v_TP_2104 * ((v_wg_id_2120 + ((4092 / v_TP_2104) * (v_l_id_2121 % (4 + v_TP_2103)))) % (4092 / v_TP_2104))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_2123 = get_local_id(0); v_l_id_2123 < v_TP_2103; v_l_id_2123 = (v_l_id_2123 + LOCAL_SIZE_0)) {
        for (int v_l_id_2124 = get_local_id(1); v_l_id_2124 < v_TP_2104; v_l_id_2124 = (v_l_id_2124 + LOCAL_SIZE_1)) {
          v__2131_0 = jacobi(v__2129[((v_l_id_2124 % v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(1 + (v_l_id_2124 % v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(2 + (v_l_id_2124 % v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(3 + (v_l_id_2124 % v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(4 + (v_l_id_2124 % v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(4 + v_TP_2104 + ((v_TP_2104 + v_l_id_2124) % v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(5 + v_TP_2104 + ((v_TP_2104 + v_l_id_2124) % v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(6 + v_TP_2104 + ((v_TP_2104 + v_l_id_2124) % v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(7 + v_TP_2104 + ((v_TP_2104 + v_l_id_2124) % v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(8 + v_TP_2104 + ((v_TP_2104 + v_l_id_2124) % v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(8 + v_l_id_2124 + (2 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(9 + v_l_id_2124 + (2 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(10 + v_l_id_2124 + (2 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(11 + v_l_id_2124 + (2 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(12 + v_l_id_2124 + (2 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(12 + v_l_id_2124 + (3 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(13 + v_l_id_2124 + (3 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(14 + v_l_id_2124 + (3 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(15 + v_l_id_2124 + (3 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(16 + v_l_id_2124 + (3 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(16 + v_l_id_2124 + (4 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(17 + v_l_id_2124 + (4 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(18 + v_l_id_2124 + (4 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(19 + v_l_id_2124 + (4 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))], v__2129[(20 + v_l_id_2124 + (4 * v_TP_2104) + (4 * v_l_id_2123) + (v_TP_2104 * v_l_id_2123))]); 
          v__2132[(8194 + v_l_id_2124 + (-4096 * ((v_l_id_2124 + (v_TP_2104 * ((v_wg_id_2120 + ((4092 / v_TP_2104) * ((v_l_id_2123 + (v_TP_2103 * v_wg_id_2120)) % v_TP_2103))) % (4092 / v_TP_2104)))) / 4095)) + (4096 * v_TP_2103 * v_wg_id_2119) + (-16777216 * (((((4092 / v_TP_2104) * ((v_l_id_2123 + (v_TP_2103 * v_wg_id_2120)) % v_TP_2103)) / (4092 / v_TP_2104)) + (v_wg_id_2120 / (4092 / v_TP_2104)) + (v_TP_2103 * v_wg_id_2119)) / 4095)) + (4096 * (((4092 / v_TP_2104) * ((v_l_id_2123 + (v_TP_2103 * v_wg_id_2120)) % v_TP_2103)) / (4092 / v_TP_2104))) + (4096 * (v_wg_id_2120 / (4092 / v_TP_2104))) + (v_TP_2104 * ((v_wg_id_2120 + ((4092 / v_TP_2104) * ((v_l_id_2123 + (v_TP_2103 * v_wg_id_2120)) % v_TP_2103))) % (4092 / v_TP_2104))))] = id(v__2131_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


