
// High-level hash: c720d531d84c51f53e6069501314edc331cd4463c76acd4222a33cce8c14745f
// Low-level hash: efac657dd46198f5f1d21c19f95a005edfb03ef6e24e47d7e27c309a030910d8

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_142" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"
#atf::tp name "v_TP_141" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__298, global float* v__302){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__301_0;
  
  for (int v_wg_id_294 = get_group_id(0); v_wg_id_294 < (4092 / v_TP_141); v_wg_id_294 = (v_wg_id_294 + NUM_GROUPS_0)) {
    for (int v_wg_id_295 = get_group_id(1); v_wg_id_295 < (4092 / v_TP_142); v_wg_id_295 = (v_wg_id_295 + NUM_GROUPS_1)) {
      for (int v_l_id_296 = get_local_id(0); v_l_id_296 < v_TP_141; v_l_id_296 = (v_l_id_296 + LOCAL_SIZE_0)) {
        for (int v_l_id_297 = get_local_id(1); v_l_id_297 < v_TP_142; v_l_id_297 = (v_l_id_297 + LOCAL_SIZE_1)) {
          v__301_0 = jacobi(v__298[((v_l_id_297 % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (4096 * (((4092 / v_TP_142) * (v_l_id_296 % (4 + v_TP_141))) / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * (v_l_id_296 % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(1 + (v_l_id_297 % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (4096 * (((4092 / v_TP_142) * (v_l_id_296 % (4 + v_TP_141))) / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * (v_l_id_296 % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(2 + (v_l_id_297 % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (4096 * (((4092 / v_TP_142) * (v_l_id_296 % (4 + v_TP_141))) / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * (v_l_id_296 % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(3 + (v_l_id_297 % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (4096 * (((4092 / v_TP_142) * (v_l_id_296 % (4 + v_TP_141))) / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * (v_l_id_296 % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(4 + (v_l_id_297 % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (4096 * (((4092 / v_TP_142) * (v_l_id_296 % (4 + v_TP_141))) / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * (v_l_id_296 % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(((v_TP_142 + v_l_id_297) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((1 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((1 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(1 + ((v_TP_142 + v_l_id_297) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((1 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((1 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(2 + ((v_TP_142 + v_l_id_297) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((1 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((1 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(3 + ((v_TP_142 + v_l_id_297) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((1 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((1 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(4 + ((v_TP_142 + v_l_id_297) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((1 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((1 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(((v_l_id_297 + (2 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((2 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((2 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(1 + ((v_l_id_297 + (2 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((2 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((2 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(2 + ((v_l_id_297 + (2 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((2 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((2 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(3 + ((v_l_id_297 + (2 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((2 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((2 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(4 + ((v_l_id_297 + (2 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((2 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((2 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(((v_l_id_297 + (3 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((3 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((3 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(1 + ((v_l_id_297 + (3 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((3 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((3 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(2 + ((v_l_id_297 + (3 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((3 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((3 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(3 + ((v_l_id_297 + (3 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((3 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((3 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(4 + ((v_l_id_297 + (3 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((3 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((3 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(((v_l_id_297 + (4 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((4 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((4 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(1 + ((v_l_id_297 + (4 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((4 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((4 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(2 + ((v_l_id_297 + (4 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((4 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((4 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(3 + ((v_l_id_297 + (4 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((4 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((4 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))], v__298[(4 + ((v_l_id_297 + (4 * v_TP_142)) % v_TP_142) + (4096 * v_TP_141 * v_wg_id_294) + (4096 * (((4092 / v_TP_142) * ((4 + v_l_id_296) % (4 + v_TP_141))) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((4 + v_l_id_296) % (4 + v_TP_141)))) % (4092 / v_TP_142))))]); 
          v__302[(8194 + v_l_id_297 + (-4096 * ((v_l_id_297 + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((v_l_id_296 + (v_TP_141 * v_wg_id_295)) % v_TP_141))) % (4092 / v_TP_142)))) / 4095)) + (4096 * v_TP_141 * v_wg_id_294) + (-16777216 * (((((4092 / v_TP_142) * ((v_l_id_296 + (v_TP_141 * v_wg_id_295)) % v_TP_141)) / (4092 / v_TP_142)) + (v_wg_id_295 / (4092 / v_TP_142)) + (v_TP_141 * v_wg_id_294)) / 4095)) + (4096 * (((4092 / v_TP_142) * ((v_l_id_296 + (v_TP_141 * v_wg_id_295)) % v_TP_141)) / (4092 / v_TP_142))) + (4096 * (v_wg_id_295 / (4092 / v_TP_142))) + (v_TP_142 * ((v_wg_id_295 + ((4092 / v_TP_142) * ((v_l_id_296 + (v_TP_141 * v_wg_id_295)) % v_TP_141))) % (4092 / v_TP_142))))] = id(v__301_0); 
        }
      }
    }
  }
}}


