
// High-level hash: 660cae58392acd1b84ff4618386905f6b02940bfec7a333667bc0e3cffc65801
// Low-level hash: 35622553abbd754935e35576576fb6667b70f7e0f61d39cc87345be8d01577cb

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float heat(float C, float S, float N, float E, float W, float B, float F){
  return 0.125f * (B - 2.0f * C + F) +
       0.125f * (S - 2.0f * C + N) +
       0.125f * (E - 2.0f * C + W) + C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__73, global float* v__85){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__84_0;
  
  for (int v_gl_id_63 = get_global_id(0); v_gl_id_63 < 254; v_gl_id_63 = (v_gl_id_63 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_64 = get_global_id(1); v_gl_id_64 < 254; v_gl_id_64 = (v_gl_id_64 + GLOBAL_SIZE_1)) {
      /* map_seq */
      for (int v_i_65 = 0; v_i_65 < 254; v_i_65 = (1 + v_i_65)) {
        v__84_0 = heat(v__73[(65793 + v_i_65 + (65536 * v_gl_id_63) + (256 * v_gl_id_64))], v__73[(65537 + v_i_65 + (65536 * v_gl_id_63) + (256 * v_gl_id_64))], v__73[(66049 + v_i_65 + (65536 * v_gl_id_63) + (256 * v_gl_id_64))], v__73[(65794 + v_i_65 + (65536 * v_gl_id_63) + (256 * v_gl_id_64))], v__73[(65792 + v_i_65 + (256 * v_gl_id_64) + (65536 * v_gl_id_63))], v__73[(257 + v_i_65 + (256 * v_gl_id_64) + (65536 * v_gl_id_63))], v__73[(131329 + v_i_65 + (65536 * v_gl_id_63) + (256 * v_gl_id_64))]); 
        v__85[(65793 + v_i_65 + (65536 * v_gl_id_63) + (256 * v_gl_id_64))] = id(v__84_0); 
      }
      /* end map_seq */
    }
  }
}}


