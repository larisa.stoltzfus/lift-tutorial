// High-level hash: 51aef87c708aed301893a9dd65c0bf0d2b526646375dc9b4702b3772ee220a58
// Low-level hash: d5a80ef8ac700599a7c5d9656653f0366daf69bcd59e39db89dfdfa796a1dd3a
float heat(float C, float S, float N, float E, float W, float B, float F){
  return 0.125f * (B - 2.0f * C + F) +
       0.125f * (S - 2.0f * C + N) +
       0.125f * (E - 2.0f * C + W) + C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__27, global float* v__30){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__29; 
  for (int v_gl_id_24 = get_global_id(2); v_gl_id_24 < 510; v_gl_id_24 = (v_gl_id_24 + 512)) {
    for (int v_gl_id_25 = get_global_id(1); v_gl_id_25 < 510; v_gl_id_25 = (v_gl_id_25 + 512)) {
      for (int v_gl_id_26 = get_global_id(0); v_gl_id_26 < 510; v_gl_id_26 = (v_gl_id_26 + 256)) {
        v__29 = heat(v__27[(262657 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(262145 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(263169 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(262658 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(262656 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(513 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(524801 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))]); 
        v__30[(262657 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))] = id(v__29); 
      }
    }
  }
}}
