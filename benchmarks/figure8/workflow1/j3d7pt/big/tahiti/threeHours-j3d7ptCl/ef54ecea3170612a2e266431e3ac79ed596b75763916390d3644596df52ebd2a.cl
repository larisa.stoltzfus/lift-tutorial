
// High-level hash: 347dba5c50784134c3bde8108ef19da1b4a1add9f8e4c5be45cb7203a96a315d
// Low-level hash: ef54ecea3170612a2e266431e3ac79ed596b75763916390d3644596df52ebd2a

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(10800)"


#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,512)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,512)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(134217728)"
#atf::ocl::input "atf::buffer<float>(134217728)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"


float jacobi(float C, float N, float S, float E, float W, float F, float B){
  return 0.161f * E + 0.162f * W +
  0.163f * S + 0.164f * N +
  0.165f * B + 0.166f * F -
  1.67f * C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__27, global float* v__30){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__29; 
  for (int v_gl_id_24 = get_global_id(2); v_gl_id_24 < 510; v_gl_id_24 = (v_gl_id_24 + GLOBAL_SIZE_2)) {
    for (int v_gl_id_25 = get_global_id(1); v_gl_id_25 < 510; v_gl_id_25 = (v_gl_id_25 + GLOBAL_SIZE_1)) {
      for (int v_gl_id_26 = get_global_id(0); v_gl_id_26 < 510; v_gl_id_26 = (v_gl_id_26 + GLOBAL_SIZE_0)) {
        v__29 = jacobi(v__27[(262657 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(262145 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(263169 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(262658 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(262656 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(513 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(524801 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))]); 
        v__30[(262657 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))] = id(v__29); 
      }
    }
  }
}}


