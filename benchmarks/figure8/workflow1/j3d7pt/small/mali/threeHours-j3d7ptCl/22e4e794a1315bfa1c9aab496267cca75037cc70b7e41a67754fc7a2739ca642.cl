
// High-level hash: 134ba95f430b3b2f339c99f0eaf746a910c5005430cef6c4f89ba212bbfcc835
// Low-level hash: 22e4e794a1315bfa1c9aab496267cca75037cc70b7e41a67754fc7a2739ca642

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"


float jacobi(float C, float N, float S, float E, float W, float F, float B){
  return 0.161f * E + 0.162f * W +
  0.163f * S + 0.164f * N +
  0.165f * B + 0.166f * F -
  1.67f * C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__61, global float* v__64){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__63; 
  for (int v_gl_id_58 = get_global_id(0); v_gl_id_58 < 254; v_gl_id_58 = (v_gl_id_58 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_59 = get_global_id(1); v_gl_id_59 < 254; v_gl_id_59 = (v_gl_id_59 + GLOBAL_SIZE_1)) {
      for (int v_gl_id_60 = get_global_id(2); v_gl_id_60 < 254; v_gl_id_60 = (v_gl_id_60 + GLOBAL_SIZE_2)) {
        v__63 = jacobi(v__61[(65793 + v_gl_id_60 + (65536 * v_gl_id_58) + (256 * v_gl_id_59))], v__61[(65537 + v_gl_id_60 + (65536 * v_gl_id_58) + (256 * v_gl_id_59))], v__61[(66049 + v_gl_id_60 + (65536 * v_gl_id_58) + (256 * v_gl_id_59))], v__61[(65794 + v_gl_id_60 + (65536 * v_gl_id_58) + (256 * v_gl_id_59))], v__61[(65792 + v_gl_id_60 + (256 * v_gl_id_59) + (65536 * v_gl_id_58))], v__61[(257 + v_gl_id_60 + (256 * v_gl_id_59) + (65536 * v_gl_id_58))], v__61[(131329 + v_gl_id_60 + (65536 * v_gl_id_58) + (256 * v_gl_id_59))]); 
        v__64[(65793 + v_gl_id_60 + (65536 * v_gl_id_58) + (256 * v_gl_id_59))] = id(v__63); 
      }
    }
  }
}}


