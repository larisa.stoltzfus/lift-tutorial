
// High-level hash: 878272a50a7cdc6b898574948ab494a1a3ce969d22ad27766f8b8d411db7df97
// Low-level hash: 6ff15ad6fff135b689ff5302dbcca88f2988586c73a8993d789afa23094eb10c

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_863" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_862" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__882, global float* v__886){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__883[(4+(2*v_TP_862)+(2*v_TP_863)+(v_TP_862*v_TP_863))];
  /* Typed Value memory */
  /* Private Memory */
  float v__885_0;
  
  for (int v_wg_id_876 = get_group_id(0); v_wg_id_876 < (4094 / v_TP_862); v_wg_id_876 = (v_wg_id_876 + NUM_GROUPS_0)) {
    for (int v_wg_id_877 = get_group_id(1); v_wg_id_877 < (4094 / v_TP_863); v_wg_id_877 = (v_wg_id_877 + NUM_GROUPS_1)) {
      for (int v_l_id_878 = get_local_id(0); v_l_id_878 < (2 + v_TP_862); v_l_id_878 = (v_l_id_878 + LOCAL_SIZE_0)) {
        for (int v_l_id_879 = get_local_id(1); v_l_id_879 < (2 + v_TP_863); v_l_id_879 = (v_l_id_879 + LOCAL_SIZE_1)) {
          v__883[(v_l_id_879 + (2 * v_l_id_878) + (v_TP_863 * v_l_id_878))] = idfloat(v__882[(v_l_id_879 + (4096 * v_TP_862 * v_wg_id_876) + (4096 * (v_wg_id_877 / (4094 / v_TP_863))) + (4096 * (((4094 / v_TP_863) * (v_l_id_878 % (2 + v_TP_862))) / (4094 / v_TP_863))) + (v_TP_863 * ((v_wg_id_877 + ((4094 / v_TP_863) * (v_l_id_878 % (2 + v_TP_862)))) % (4094 / v_TP_863))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_880 = get_local_id(0); v_l_id_880 < v_TP_862; v_l_id_880 = (v_l_id_880 + LOCAL_SIZE_0)) {
        for (int v_l_id_881 = get_local_id(1); v_l_id_881 < v_TP_863; v_l_id_881 = (v_l_id_881 + LOCAL_SIZE_1)) {
          v__885_0 = grad(v__883[(1 + (v_l_id_881 % v_TP_863) + (2 * v_l_id_880) + (v_TP_863 * v_l_id_880))], v__883[(5 + v_l_id_881 + (2 * v_TP_863) + (2 * v_l_id_880) + (v_TP_863 * v_l_id_880))], v__883[(2 + v_TP_863 + ((v_TP_863 + v_l_id_881) % v_TP_863) + (2 * v_l_id_880) + (v_TP_863 * v_l_id_880))], v__883[(4 + v_TP_863 + ((v_TP_863 + v_l_id_881) % v_TP_863) + (2 * v_l_id_880) + (v_TP_863 * v_l_id_880))], v__883[(3 + v_TP_863 + ((v_TP_863 + v_l_id_881) % v_TP_863) + (2 * v_l_id_880) + (v_TP_863 * v_l_id_880))]); 
          v__886[(4097 + v_l_id_881 + (-4096 * ((v_l_id_881 + (v_TP_863 * ((v_wg_id_877 + ((4094 / v_TP_863) * ((v_l_id_880 + (v_TP_862 * v_wg_id_877)) % v_TP_862))) % (4094 / v_TP_863)))) / 4095)) + (4096 * v_TP_862 * v_wg_id_876) + (-16777216 * (((((4094 / v_TP_863) * ((v_l_id_880 + (v_TP_862 * v_wg_id_877)) % v_TP_862)) / (4094 / v_TP_863)) + (v_wg_id_877 / (4094 / v_TP_863)) + (v_TP_862 * v_wg_id_876)) / 4095)) + (4096 * (((4094 / v_TP_863) * ((v_l_id_880 + (v_TP_862 * v_wg_id_877)) % v_TP_862)) / (4094 / v_TP_863))) + (4096 * (v_wg_id_877 / (4094 / v_TP_863))) + (v_TP_863 * ((v_wg_id_877 + ((4094 / v_TP_863) * ((v_l_id_880 + (v_TP_862 * v_wg_id_877)) % v_TP_862))) % (4094 / v_TP_863))))] = id(v__885_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


