
// High-level hash: 878272a50a7cdc6b898574948ab494a1a3ce969d22ad27766f8b8d411db7df97
// Low-level hash: 80ea1af9dba5b07bf2b8b5a11fa1bd671981347e38bb105386661e85d778fbe4

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_209" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_208" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__224, global float* v__227){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__226_0;
  
  for (int v_wg_id_220 = get_group_id(1); v_wg_id_220 < (4094 / v_TP_208); v_wg_id_220 = (v_wg_id_220 + NUM_GROUPS_1)) {
    for (int v_wg_id_221 = get_group_id(0); v_wg_id_221 < (4094 / v_TP_209); v_wg_id_221 = (v_wg_id_221 + NUM_GROUPS_0)) {
      for (int v_l_id_222 = get_local_id(1); v_l_id_222 < v_TP_208; v_l_id_222 = (v_l_id_222 + LOCAL_SIZE_1)) {
        for (int v_l_id_223 = get_local_id(0); v_l_id_223 < v_TP_209; v_l_id_223 = (v_l_id_223 + LOCAL_SIZE_0)) {
          v__226_0 = grad(v__224[(1 + (v_l_id_223 % v_TP_209) + (4096 * v_TP_208 * v_wg_id_220) + (4096 * (v_wg_id_221 / (4094 / v_TP_209))) + (4096 * (((4094 / v_TP_209) * (v_l_id_222 % (2 + v_TP_208))) / (4094 / v_TP_209))) + (v_TP_209 * ((v_wg_id_221 + ((4094 / v_TP_209) * (v_l_id_222 % (2 + v_TP_208)))) % (4094 / v_TP_209))))], v__224[(1 + ((v_l_id_223 + (2 * v_TP_209)) % v_TP_209) + (4096 * v_TP_208 * v_wg_id_220) + (4096 * (((4094 / v_TP_209) * ((2 + v_l_id_222) % (2 + v_TP_208))) / (4094 / v_TP_209))) + (4096 * (v_wg_id_221 / (4094 / v_TP_209))) + (v_TP_209 * ((v_wg_id_221 + ((4094 / v_TP_209) * ((2 + v_l_id_222) % (2 + v_TP_208)))) % (4094 / v_TP_209))))], v__224[(((v_TP_209 + v_l_id_223) % v_TP_209) + (4096 * v_TP_208 * v_wg_id_220) + (4096 * (((4094 / v_TP_209) * ((1 + v_l_id_222) % (2 + v_TP_208))) / (4094 / v_TP_209))) + (4096 * (v_wg_id_221 / (4094 / v_TP_209))) + (v_TP_209 * ((v_wg_id_221 + ((4094 / v_TP_209) * ((1 + v_l_id_222) % (2 + v_TP_208)))) % (4094 / v_TP_209))))], v__224[(2 + ((v_TP_209 + v_l_id_223) % v_TP_209) + (4096 * v_TP_208 * v_wg_id_220) + (4096 * (((4094 / v_TP_209) * ((1 + v_l_id_222) % (2 + v_TP_208))) / (4094 / v_TP_209))) + (4096 * (v_wg_id_221 / (4094 / v_TP_209))) + (v_TP_209 * ((v_wg_id_221 + ((4094 / v_TP_209) * ((1 + v_l_id_222) % (2 + v_TP_208)))) % (4094 / v_TP_209))))], v__224[(1 + ((v_TP_209 + v_l_id_223) % v_TP_209) + (4096 * v_TP_208 * v_wg_id_220) + (4096 * (((4094 / v_TP_209) * ((1 + v_l_id_222) % (2 + v_TP_208))) / (4094 / v_TP_209))) + (4096 * (v_wg_id_221 / (4094 / v_TP_209))) + (v_TP_209 * ((v_wg_id_221 + ((4094 / v_TP_209) * ((1 + v_l_id_222) % (2 + v_TP_208)))) % (4094 / v_TP_209))))]); 
          v__227[(4097 + v_l_id_223 + (-4096 * ((v_l_id_223 + (v_TP_209 * ((v_wg_id_221 + ((4094 / v_TP_209) * ((v_l_id_222 + (v_TP_208 * v_wg_id_221)) % v_TP_208))) % (4094 / v_TP_209)))) / 4095)) + (4096 * v_TP_208 * v_wg_id_220) + (-16777216 * (((((4094 / v_TP_209) * ((v_l_id_222 + (v_TP_208 * v_wg_id_221)) % v_TP_208)) / (4094 / v_TP_209)) + (v_wg_id_221 / (4094 / v_TP_209)) + (v_TP_208 * v_wg_id_220)) / 4095)) + (4096 * (((4094 / v_TP_209) * ((v_l_id_222 + (v_TP_208 * v_wg_id_221)) % v_TP_208)) / (4094 / v_TP_209))) + (4096 * (v_wg_id_221 / (4094 / v_TP_209))) + (v_TP_209 * ((v_wg_id_221 + ((4094 / v_TP_209) * ((v_l_id_222 + (v_TP_208 * v_wg_id_221)) % v_TP_208))) % (4094 / v_TP_209))))] = id(v__226_0); 
        }
      }
    }
  }
}}


