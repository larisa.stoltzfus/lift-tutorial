
// High-level hash: 878272a50a7cdc6b898574948ab494a1a3ce969d22ad27766f8b8d411db7df97
// Low-level hash: b3f4969b5eac5ef76b04f1fa21cf514a33c5b1988304ecf6a1be8f4ed1101894

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_300" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_299" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__358, global float* v__374){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__370[(4+(2*v_TP_299)+(2*v_TP_300)+(v_TP_299*v_TP_300))];
  /* Typed Value memory */
  /* Private Memory */
  float v__372_0;
  
  for (int v_wg_id_351 = get_group_id(1); v_wg_id_351 < (4094 / v_TP_299); v_wg_id_351 = (v_wg_id_351 + NUM_GROUPS_1)) {
    for (int v_wg_id_352 = get_group_id(0); v_wg_id_352 < (4094 / v_TP_300); v_wg_id_352 = (v_wg_id_352 + NUM_GROUPS_0)) {
      for (int v_l_id_354 = get_local_id(1); v_l_id_354 < (2 + v_TP_299); v_l_id_354 = (v_l_id_354 + LOCAL_SIZE_1)) {
        for (int v_l_id_355 = get_local_id(0); v_l_id_355 < (2 + v_TP_300); v_l_id_355 = (v_l_id_355 + LOCAL_SIZE_0)) {
          v__370[(v_l_id_355 + (2 * v_l_id_354) + (v_TP_300 * v_l_id_354))] = idfloat(v__358[(v_l_id_355 + (4096 * v_TP_299 * v_wg_id_351) + (4096 * (v_wg_id_352 / (4094 / v_TP_300))) + (4096 * (((4094 / v_TP_300) * (v_l_id_354 % (2 + v_TP_299))) / (4094 / v_TP_300))) + (v_TP_300 * ((v_wg_id_352 + ((4094 / v_TP_300) * (v_l_id_354 % (2 + v_TP_299)))) % (4094 / v_TP_300))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_356 = get_local_id(1); v_l_id_356 < v_TP_299; v_l_id_356 = (v_l_id_356 + LOCAL_SIZE_1)) {
        for (int v_l_id_357 = get_local_id(0); v_l_id_357 < v_TP_300; v_l_id_357 = (v_l_id_357 + LOCAL_SIZE_0)) {
          v__372_0 = grad(v__370[(1 + (v_l_id_357 % v_TP_300) + (2 * v_l_id_356) + (v_TP_300 * v_l_id_356))], v__370[(5 + v_l_id_357 + (2 * v_TP_300) + (2 * v_l_id_356) + (v_TP_300 * v_l_id_356))], v__370[(2 + v_TP_300 + ((v_TP_300 + v_l_id_357) % v_TP_300) + (2 * v_l_id_356) + (v_TP_300 * v_l_id_356))], v__370[(4 + v_TP_300 + ((v_TP_300 + v_l_id_357) % v_TP_300) + (2 * v_l_id_356) + (v_TP_300 * v_l_id_356))], v__370[(3 + v_TP_300 + ((v_TP_300 + v_l_id_357) % v_TP_300) + (2 * v_l_id_356) + (v_TP_300 * v_l_id_356))]); 
          v__374[(4097 + v_l_id_357 + (-4096 * ((v_l_id_357 + (v_TP_300 * ((v_wg_id_352 + ((4094 / v_TP_300) * ((v_l_id_356 + (v_TP_299 * v_wg_id_352)) % v_TP_299))) % (4094 / v_TP_300)))) / 4095)) + (4096 * v_TP_299 * v_wg_id_351) + (-16777216 * (((((4094 / v_TP_300) * ((v_l_id_356 + (v_TP_299 * v_wg_id_352)) % v_TP_299)) / (4094 / v_TP_300)) + (v_wg_id_352 / (4094 / v_TP_300)) + (v_TP_299 * v_wg_id_351)) / 4095)) + (4096 * (((4094 / v_TP_300) * ((v_l_id_356 + (v_TP_299 * v_wg_id_352)) % v_TP_299)) / (4094 / v_TP_300))) + (4096 * (v_wg_id_352 / (4094 / v_TP_300))) + (v_TP_300 * ((v_wg_id_352 + ((4094 / v_TP_300) * ((v_l_id_356 + (v_TP_299 * v_wg_id_352)) % v_TP_299))) % (4094 / v_TP_300))))] = id(v__372_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


