
// High-level hash: 3e86bf4262017665dc53d7c0145e6fa20a7301215072af4776a8b0c858fb2a5d
// Low-level hash: b3f4969b5eac5ef76b04f1fa21cf514a33c5b1988304ecf6a1be8f4ed1101894

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_284" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_283" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__336, global float* v__355){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__351[(4+(2*v_TP_283)+(2*v_TP_284)+(v_TP_283*v_TP_284))];
  /* Typed Value memory */
  /* Private Memory */
  float v__354_0;
  
  for (int v_wg_id_329 = get_group_id(1); v_wg_id_329 < (4094 / v_TP_283); v_wg_id_329 = (v_wg_id_329 + NUM_GROUPS_1)) {
    for (int v_wg_id_330 = get_group_id(0); v_wg_id_330 < (4094 / v_TP_284); v_wg_id_330 = (v_wg_id_330 + NUM_GROUPS_0)) {
      for (int v_l_id_331 = get_local_id(1); v_l_id_331 < (2 + v_TP_283); v_l_id_331 = (v_l_id_331 + LOCAL_SIZE_1)) {
        for (int v_l_id_333 = get_local_id(0); v_l_id_333 < (2 + v_TP_284); v_l_id_333 = (v_l_id_333 + LOCAL_SIZE_0)) {
          v__351[(v_l_id_333 + (2 * v_l_id_331) + (v_TP_284 * v_l_id_331))] = idfloat(v__336[(v_l_id_333 + (4096 * v_TP_283 * v_wg_id_329) + (4096 * (v_wg_id_330 / (4094 / v_TP_284))) + (4096 * (((4094 / v_TP_284) * (v_l_id_331 % (2 + v_TP_283))) / (4094 / v_TP_284))) + (v_TP_284 * ((v_wg_id_330 + ((4094 / v_TP_284) * (v_l_id_331 % (2 + v_TP_283)))) % (4094 / v_TP_284))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_334 = get_local_id(1); v_l_id_334 < v_TP_283; v_l_id_334 = (v_l_id_334 + LOCAL_SIZE_1)) {
        for (int v_l_id_335 = get_local_id(0); v_l_id_335 < v_TP_284; v_l_id_335 = (v_l_id_335 + LOCAL_SIZE_0)) {
          v__354_0 = grad(v__351[(1 + (v_l_id_335 % v_TP_284) + (2 * v_l_id_334) + (v_TP_284 * v_l_id_334))], v__351[(5 + v_l_id_335 + (2 * v_TP_284) + (2 * v_l_id_334) + (v_TP_284 * v_l_id_334))], v__351[(2 + v_TP_284 + ((v_TP_284 + v_l_id_335) % v_TP_284) + (2 * v_l_id_334) + (v_TP_284 * v_l_id_334))], v__351[(4 + v_TP_284 + ((v_TP_284 + v_l_id_335) % v_TP_284) + (2 * v_l_id_334) + (v_TP_284 * v_l_id_334))], v__351[(3 + v_TP_284 + ((v_TP_284 + v_l_id_335) % v_TP_284) + (2 * v_l_id_334) + (v_TP_284 * v_l_id_334))]); 
          v__355[(4097 + v_l_id_335 + (-4096 * ((v_l_id_335 + (v_TP_284 * ((v_wg_id_330 + ((4094 / v_TP_284) * ((v_l_id_334 + (v_TP_283 * v_wg_id_330)) % v_TP_283))) % (4094 / v_TP_284)))) / 4095)) + (4096 * v_TP_283 * v_wg_id_329) + (-16777216 * (((((4094 / v_TP_284) * ((v_l_id_334 + (v_TP_283 * v_wg_id_330)) % v_TP_283)) / (4094 / v_TP_284)) + (v_wg_id_330 / (4094 / v_TP_284)) + (v_TP_283 * v_wg_id_329)) / 4095)) + (4096 * (((4094 / v_TP_284) * ((v_l_id_334 + (v_TP_283 * v_wg_id_330)) % v_TP_283)) / (4094 / v_TP_284))) + (4096 * (v_wg_id_330 / (4094 / v_TP_284))) + (v_TP_284 * ((v_wg_id_330 + ((4094 / v_TP_284) * ((v_l_id_334 + (v_TP_283 * v_wg_id_330)) % v_TP_283))) % (4094 / v_TP_284))))] = id(v__354_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


