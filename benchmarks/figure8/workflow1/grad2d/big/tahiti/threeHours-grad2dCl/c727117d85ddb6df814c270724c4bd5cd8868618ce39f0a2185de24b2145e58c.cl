
// High-level hash: 90c18c8a78a82c9e9f620124d26c4f8d01f5527610d104730b0f5de20d24c309
// Low-level hash: c727117d85ddb6df814c270724c4bd5cd8868618ce39f0a2185de24b2145e58c

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_47" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_46" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float grad(float top, float bottom, float left, float right, float center){
  return center + 1.0f/sqrt(0.0001f +
 (center-top)*(center-top) +
 (center-bottom)*(center-bottom) +
 (center-right)*(center-right) +
 (center-left)*(center-left));
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__68, global float* v__77){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__76_0;
  
  for (int v_gl_id_64 = get_global_id(1); v_gl_id_64 < (8190 / v_TP_46); v_gl_id_64 = (v_gl_id_64 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_65 = get_global_id(0); v_gl_id_65 < (8190 / v_TP_47); v_gl_id_65 = (v_gl_id_65 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_66 = 0; v_i_66 < v_TP_46; v_i_66 = (1 + v_i_66)) {
        /* map_seq */
        for (int v_i_67 = 0; v_i_67 < v_TP_47; v_i_67 = (1 + v_i_67)) {
          v__76_0 = grad(v__68[(1 + (v_i_67 % v_TP_47) + (8192 * v_TP_46 * v_gl_id_64) + (8192 * (((8190 / v_TP_47) * (v_i_66 % (2 + v_TP_46))) / (8190 / v_TP_47))) + (8192 * (v_gl_id_65 / (8190 / v_TP_47))) + (v_TP_47 * ((v_gl_id_65 + ((8190 / v_TP_47) * (v_i_66 % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__68[(1 + ((v_i_67 + (2 * v_TP_47)) % v_TP_47) + (8192 * v_TP_46 * v_gl_id_64) + (8192 * (v_gl_id_65 / (8190 / v_TP_47))) + (8192 * (((8190 / v_TP_47) * ((2 + v_i_66) % (2 + v_TP_46))) / (8190 / v_TP_47))) + (v_TP_47 * ((v_gl_id_65 + ((8190 / v_TP_47) * ((2 + v_i_66) % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__68[(((v_TP_47 + v_i_67) % v_TP_47) + (8192 * v_TP_46 * v_gl_id_64) + (8192 * (v_gl_id_65 / (8190 / v_TP_47))) + (8192 * (((8190 / v_TP_47) * ((1 + v_i_66) % (2 + v_TP_46))) / (8190 / v_TP_47))) + (v_TP_47 * ((v_gl_id_65 + ((8190 / v_TP_47) * ((1 + v_i_66) % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__68[(2 + ((v_TP_47 + v_i_67) % v_TP_47) + (8192 * v_TP_46 * v_gl_id_64) + (8192 * (v_gl_id_65 / (8190 / v_TP_47))) + (8192 * (((8190 / v_TP_47) * ((1 + v_i_66) % (2 + v_TP_46))) / (8190 / v_TP_47))) + (v_TP_47 * ((v_gl_id_65 + ((8190 / v_TP_47) * ((1 + v_i_66) % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__68[(1 + ((v_TP_47 + v_i_67) % v_TP_47) + (8192 * v_TP_46 * v_gl_id_64) + (8192 * (v_gl_id_65 / (8190 / v_TP_47))) + (8192 * (((8190 / v_TP_47) * ((1 + v_i_66) % (2 + v_TP_46))) / (8190 / v_TP_47))) + (v_TP_47 * ((v_gl_id_65 + ((8190 / v_TP_47) * ((1 + v_i_66) % (2 + v_TP_46)))) % (8190 / v_TP_47))))]); 
          v__77[(8193 + v_i_67 + (8192 * v_TP_46 * v_gl_id_64) + (-67108864 * (((v_gl_id_65 / (8190 / v_TP_47)) + (((8190 / v_TP_47) * ((v_i_66 + (v_TP_46 * v_gl_id_65)) % v_TP_46)) / (8190 / v_TP_47)) + (v_TP_46 * v_gl_id_64)) / 8191)) + (-8192 * ((v_i_67 + (v_TP_47 * ((v_gl_id_65 + ((8190 / v_TP_47) * ((v_i_66 + (v_TP_46 * v_gl_id_65)) % v_TP_46))) % (8190 / v_TP_47)))) / 8191)) + (8192 * (v_gl_id_65 / (8190 / v_TP_47))) + (8192 * (((8190 / v_TP_47) * ((v_i_66 + (v_TP_46 * v_gl_id_65)) % v_TP_46)) / (8190 / v_TP_47))) + (v_TP_47 * ((v_gl_id_65 + ((8190 / v_TP_47) * ((v_i_66 + (v_TP_46 * v_gl_id_65)) % v_TP_46))) % (8190 / v_TP_47))))] = id(v__76_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


