// High-level hash: fa2766cf081e60976c85fc2be011368f23dc2d03ee4a10ee662dfe4702eb3524
// Low-level hash: c0b1c2ba70ecd1c09b521fe6ac9515f3c8fd7364ebae817d7477ea8b500b6816
float jacobi(float C, float N, float S, float E, float W, float F, float B, float FN, float BN, float FS, float BS, float FW, float BW, float NW, float SW, float FE, float BE, float NE, float SE){
  return 2.666f * C - 0.166f * (F + B + N + S + E + W) -
       0.0833f * (FN + BN + FS + BS + FW + BW +
                  NW + SW + FE + BE + NE + SE);
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__56, global float* v__65){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__64; 
  for (int v_gl_id_40 = get_global_id(2); v_gl_id_40 < 254; v_gl_id_40 = (v_gl_id_40 + 128)) {
    for (int v_gl_id_52 = get_global_id(1); v_gl_id_52 < 254; v_gl_id_52 = (v_gl_id_52 + 256)) {
      for (int v_gl_id_54 = get_global_id(0); v_gl_id_54 < 254; v_gl_id_54 = (v_gl_id_54 + 8)) {
        v__64 = jacobi(v__56[(65793 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))], v__56[(65537 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))], v__56[(66049 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))], v__56[(65794 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))], v__56[(65792 + v_gl_id_54 + (256 * v_gl_id_52) + (65536 * v_gl_id_40))], v__56[(257 + v_gl_id_54 + (256 * v_gl_id_52) + (65536 * v_gl_id_40))], v__56[(131329 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))], v__56[(1 + v_gl_id_54 + (256 * v_gl_id_52) + (65536 * v_gl_id_40))], v__56[(131073 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))], v__56[(513 + v_gl_id_54 + (256 * v_gl_id_52) + (65536 * v_gl_id_40))], v__56[(131585 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))], v__56[(256 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))], v__56[(131328 + v_gl_id_54 + (256 * v_gl_id_52) + (65536 * v_gl_id_40))], v__56[(65536 + v_gl_id_54 + (256 * v_gl_id_52) + (65536 * v_gl_id_40))], v__56[(66048 + v_gl_id_54 + (256 * v_gl_id_52) + (65536 * v_gl_id_40))], v__56[(258 + v_gl_id_54 + (256 * v_gl_id_52) + (65536 * v_gl_id_40))], v__56[(131330 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))], v__56[(65538 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))], v__56[(66050 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))]); 
        v__65[(65793 + v_gl_id_54 + (65536 * v_gl_id_40) + (256 * v_gl_id_52))] = id(v__64); 
      }
    }
  }
}}
