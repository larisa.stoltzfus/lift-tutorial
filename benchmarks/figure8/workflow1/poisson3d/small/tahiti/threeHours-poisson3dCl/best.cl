// High-level hash: fa2766cf081e60976c85fc2be011368f23dc2d03ee4a10ee662dfe4702eb3524
// Low-level hash: c0b1c2ba70ecd1c09b521fe6ac9515f3c8fd7364ebae817d7477ea8b500b6816
float jacobi(float C, float N, float S, float E, float W, float F, float B, float FN, float BN, float FS, float BS, float FW, float BW, float NW, float SW, float FE, float BE, float NE, float SE){
  return 2.666f * C - 0.166f * (F + B + N + S + E + W) -
       0.0833f * (FN + BN + FS + BS + FW + BW +
                  NW + SW + FE + BE + NE + SE);
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__27, global float* v__30){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__29; 
  for (int v_gl_id_24 = get_global_id(2); v_gl_id_24 < 254; v_gl_id_24 = (v_gl_id_24 + 256)) {
    for (int v_gl_id_25 = get_global_id(1); v_gl_id_25 < 254; v_gl_id_25 = (v_gl_id_25 + 64)) {
      for (int v_gl_id_26 = get_global_id(0); v_gl_id_26 < 254; v_gl_id_26 = (v_gl_id_26 + 256)) {
        v__29 = jacobi(v__27[(65793 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65537 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(66049 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65794 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65792 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(257 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(131329 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(1 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(131073 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(513 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(131585 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(256 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(131328 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(65536 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(66048 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(258 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(131330 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65538 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(66050 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))]); 
        v__30[(65793 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))] = id(v__29); 
      }
    }
  }
}}
