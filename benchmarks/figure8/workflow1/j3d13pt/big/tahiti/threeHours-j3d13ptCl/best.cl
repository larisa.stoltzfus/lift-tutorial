// High-level hash: 3345e1699b85e12ecf9ecd9d3e1e281741103d7af8ffcae874e837b9058d7e7a
// Low-level hash: 11f87dca5aa3bef79e4c821eae411f51be52afe25493320de619498ca62a1fd9
float jacobi(float EE, float E, float W, float WW, float SS, float S, float N, float NN, float BB, float B, float F, float FF, float C){
  return 0.083f * EE + 0.083f * E + 0.083f * W + 0.083f * WW +
       0.083f * SS + 0.083f * S + 0.083f * N + 0.083f * NN +
       0.083f * BB + 0.083f * B + 0.083f * F + 0.083f * FF -
       0.996f * C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__27, global float* v__30){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__29; 
  for (int v_gl_id_24 = get_global_id(2); v_gl_id_24 < 508; v_gl_id_24 = (v_gl_id_24 + 8)) {
    for (int v_gl_id_25 = get_global_id(1); v_gl_id_25 < 508; v_gl_id_25 = (v_gl_id_25 + 64)) {
      for (int v_gl_id_26 = get_global_id(0); v_gl_id_26 < 508; v_gl_id_26 = (v_gl_id_26 + 16)) {
        v__29 = jacobi(v__27[(525316 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(525315 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(525313 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(525312 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(526338 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(525826 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(524802 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(524290 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(1049602 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(787458 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(263170 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))], v__27[(1026 + v_gl_id_26 + (512 * v_gl_id_25) + (262144 * v_gl_id_24))], v__27[(525314 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))]); 
        v__30[(525314 + v_gl_id_26 + (262144 * v_gl_id_24) + (512 * v_gl_id_25))] = id(v__29); 
      }
    }
  }
}}
