// High-level hash: 952ed6e585e8c66c2ee49bae315e8147db8650df17b5cb3b39f687a1192470de
// Low-level hash: e62e3ad914fcccc9759eb1f7e32aa2b540b509300e2a0ada6e3172b15370ecc3
float jacobi(float EE, float E, float W, float WW, float SS, float S, float N, float NN, float BB, float B, float F, float FF, float C){
  return 0.083f * EE + 0.083f * E + 0.083f * W + 0.083f * WW +
       0.083f * SS + 0.083f * S + 0.083f * N + 0.083f * NN +
       0.083f * BB + 0.083f * B + 0.083f * F + 0.083f * FF -
       0.996f * C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__61, global float* v__64){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__63; 
  for (int v_gl_id_51 = get_global_id(2); v_gl_id_51 < 252; v_gl_id_51 = (v_gl_id_51 + 256)) {
    for (int v_gl_id_52 = get_global_id(1); v_gl_id_52 < 252; v_gl_id_52 = (v_gl_id_52 + 128)) {
      for (int v_gl_id_53 = get_global_id(0); v_gl_id_53 < 252; v_gl_id_53 = (v_gl_id_53 + 8)) {
        v__63 = jacobi(v__61[(131588 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__61[(131587 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__61[(131585 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__61[(131584 + v_gl_id_53 + (256 * v_gl_id_52) + (65536 * v_gl_id_51))], v__61[(132098 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__61[(131842 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__61[(131330 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__61[(131074 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__61[(262658 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__61[(197122 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__61[(66050 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__61[(514 + v_gl_id_53 + (256 * v_gl_id_52) + (65536 * v_gl_id_51))], v__61[(131586 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))]); 
        v__64[(131586 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))] = id(v__63); 
      }
    }
  }
}}
