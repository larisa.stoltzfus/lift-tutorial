
// High-level hash: 952ed6e585e8c66c2ee49bae315e8147db8650df17b5cb3b39f687a1192470de
// Low-level hash: e62e3ad914fcccc9759eb1f7e32aa2b540b509300e2a0ada6e3172b15370ecc3

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(10800)"


#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"


float jacobi(float EE, float E, float W, float WW, float SS, float S, float N, float NN, float BB, float B, float F, float FF, float C){
  return 0.083f * EE + 0.083f * E + 0.083f * W + 0.083f * WW +
       0.083f * SS + 0.083f * S + 0.083f * N + 0.083f * NN +
       0.083f * BB + 0.083f * B + 0.083f * F + 0.083f * FF -
       0.996f * C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__27, global float* v__30){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__29; 
  for (int v_gl_id_24 = get_global_id(2); v_gl_id_24 < 252; v_gl_id_24 = (v_gl_id_24 + GLOBAL_SIZE_2)) {
    for (int v_gl_id_25 = get_global_id(1); v_gl_id_25 < 252; v_gl_id_25 = (v_gl_id_25 + GLOBAL_SIZE_1)) {
      for (int v_gl_id_26 = get_global_id(0); v_gl_id_26 < 252; v_gl_id_26 = (v_gl_id_26 + GLOBAL_SIZE_0)) {
        v__29 = jacobi(v__27[(131588 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(131587 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(131585 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(131584 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(132098 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(131842 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(131330 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(131074 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(262658 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(197122 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(66050 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(514 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(131586 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))]); 
        v__30[(131586 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))] = id(v__29); 
      }
    }
  }
}}


