#!/bin/sh
: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"

pushd $ROOTDIR/tools > /dev/null
wget http://gcc.parentingamerica.com/releases/gcc-7.1.0/gcc-7.1.0.tar.gz
tar -xvf gcc-7.1.0.tar.gz
pushd gcc-7.1.0 > /dev/null
./contrib/download_prerequisites
popd > /dev/null
mv gcc-7.1.0 gcc-7.1.0.src
mkdir gcc-7.1.0
pushd gcc-7.1.0 > /dev/null
../gcc-7.1.0.src/configure --prefix=$ROOTDIR/tools/gcc-7.1.0 --enable-languages=c,c++ --disable-multilib
JOBS=$(nproc --all)
make -j$JOBS
make install
