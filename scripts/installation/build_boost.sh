#!/bin/bash
: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"

pushd $ROOTDIR/tools > /dev/null
wget https://dl.bintray.com/boostorg/release/1.64.0/source/boost_1_64_0.tar.gz
tar -xvf boost_1_64_0.tar.gz
rm boost_1_64_0.tar.gz
cd boost_1_64_0.tar.gz
./bootstrap.sh
./b2
popd > /dev/null
