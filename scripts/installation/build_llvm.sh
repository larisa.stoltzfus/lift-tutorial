#!/bin/bash
: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"

pushd $ROOTDIR/tools > /dev/null
# download llvm-3.7
wget http://releases.llvm.org/3.7.0/llvm-3.7.0.src.tar.xz
tar -xvf llvm-3.7.0.src.tar.xz
rm llvm-3.7.0.src.tar.xz

# download clang-3.7
pushd llvm-3.7.0.src/tools > /dev/null
wget http://releases.llvm.org/3.7.0/cfe-3.7.0.src.tar.xz
tar -xvf cfe-3.7.0.src.tar.xz
mv cfe-3.7.0.src clang
rm cfe-3.7.0.src.tar.xz
popd > /dev/null

# build out-of-source
mkdir llvm-3.7.0
cd llvm-3.7.0
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$PWD -DLLVM_BUILD_LLVM_DYLIB=ON -DLLVM_ENABLE_RTTI=ON ../llvm-3.7.0.src
JOBS=$(nproc --all)
make -j$JOBS
make install
popd > /dev/null
