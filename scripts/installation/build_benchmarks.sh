#!/bin/bash
: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"

REFERENCE=${ROOTDIR}/benchmarks/figure7/workflow1/reference
OUTPUTDIR=${ROOTDIR}/output_data

echo "==="; echo "Build shoc benchmark: stencil2d (reference)"; echo "==="
# build shoc
pushd $SHOCPATH > /dev/null
./configure --without-cuda --with-opencl --without-mpi 2>&1 | tee ${ROOTDIR}/logs/shocconfigure.log
make 2>&1 | tee ${ROOTDIR}/logs/shocbuild.log
popd > /dev/null

# loop over reference directories 
for DIR in $REFERENCE/*; do

    cd $DIR
    BASE=$( basename "${DIR}" )

    # make clean && make - except shoc, which is a total pain
    if ! [ $BASE == "stencil2d" ]; then
        echo "==="; echo "Build benchmark: ${BASE} (reference)"; echo "==="
        make clean
        make
    fi

done


# loop over lift directories  
array=( amd mali nvidia )
for ARCH in "${array[@]}"; do
    echo "$ARCH"
    DIR="${ROOTDIR}/benchmarks/figure7/workflow1/lift/${ARCH}/acoustic"
    cd $DIR
    BASE=$( basename "${DIR}" )

    # only need to build accoustic
    echo "==="; echo "Build benchmark ${BASE} (lift)"; echo "==="
    make clean
    make
done

echo ""
echo "done!"
echo ""
