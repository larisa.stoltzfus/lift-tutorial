#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "Illegal number of parameters (enter: architecture [nvidia, amd, mali])"
    exit -1
fi

: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"
PARAM=$1
ARCH=$(echo ${PARAM} | awk '{print tolower($0)}')

echo $ARCH

REFERENCE=${ROOTDIR}/benchmarks/figure7/workflow1/reference
LIFT=${ROOTDIR}/benchmarks/figure7/workflow1/lift/${ARCH}
OUTPUTDIR=${ROOTDIR}/output_data

declare -A benchmarks
benchmarks["acoustic"]="${OCL_PLATFORM_ID} ${OCL_DEVICE_ID} ${ITERATIONS} 0"
benchmarks["hotspot"]="${OCL_PLATFORM_ID} ${OCL_DEVICE_ID} 8192 ${ITERATIONS}"
benchmarks["hotspot3D"]="${OCL_PLATFORM_ID} ${OCL_DEVICE_ID} 512 8 ${ITERATIONS}"
benchmarks["srad"]="${OCL_PLATFORM_ID} ${OCL_DEVICE_ID} ${ITERATIONS} 502 458"
benchmarks["stencil2d"]="${OCL_PLATFORM_ID} ${OCL_DEVICE_ID} ${ITERATIONS} 8192"

############################################
# 1. Run reference implementations
############################################
echo "Running reference implementations"
# loop over reference directories 
for DIR in $REFERENCE/*; do

    cd $DIR
    BASE=$( basename "${DIR}" )
    echo "This is: ${BASE}"

    # run ./benchmark.sh script
    echo "DATA: ${OUTPUTDIR}"
    echo "${benchmarks[${BASE}]}"
    ./benchmark.sh ${benchmarks[${BASE}]}

    NEWDIR=${OUTPUTDIR}/${ARCH}
    NEWREFDIR=${NEWDIR}/reference
    CPDATA="cp ${BASE}*.out ${NEWREFDIR}"

    # move data to "output_data" directory in folder with architecture 
    if [ -d "$NEWDIR" ]; then
        if [ -d "$NEWREFDIR" ]; then
            echo "Copying data ${CPDATA}"
            $(${CPDATA})
        else
            echo "Making directory ${NEWREFDIR}"
            mkdir $NEWREFDIR
            echo "Copying data ${CPDATA}"
            $(${CPDATA})
        fi
    else
        echo "Making directory ${NEWDIR}"
        mkdir $NEWDIR
        echo "Making directory ${NEWREFDIR}"
        mkdir $NEWREFDIR
        echo "Copying data ${CPDATA}"
        $(${CPDATA})
    fi

done

############################################
# 2. Run Lift implementations
############################################
echo "Running lift implementations"
# loop over lift directories  
for DIR in $LIFT/*; do

    cd $DIR
    BASE=$( basename "${DIR}" )
    echo "This is: ${BASE}"

    # run ./benchmark.sh script
    echo "${benchmarks[${BASE}]}"
    ./benchmark.sh ${benchmarks[${BASE}]}

    NEWDIR=${OUTPUTDIR}/${ARCH}
    NEWLIFTDIR=${NEWDIR}/lift
    CPDATA="cp ${BASE}*.out ${NEWLIFTDIR}"

    # move data to "output_data" directory in folder with architecture 
    if [ -d "$NEWDIR" ]; then
        if [ -d "$NEWLIFTDIR" ]; then
            echo "Copying data ${CPDATA}"
            $(${CPDATA})
        else
            echo "Making directory ${NEWLIFTDIR}"
            mkdir $NEWLIFTDIR
            echo "Copying data ${CPDATA}"
            $(${CPDATA})
        fi
    else
        echo "Making directory ${NEWDIR}"
        mkdir $NEWDIR
        echo "Making directory ${NEWLIFTDIR}"
        mkdir $NEWLIFTDIR
        echo "Copying data ${CPDATA}"
        $(${CPDATA})
    fi

done

############################################
# 3. Plot results
############################################
cd $ROOTDIR/scripts/r_scripts
Rscript plotBestHandwritten.r

echo ""
echo "done! workflow1-figure7.sh has finished"
echo ""
