#!/bin/bash

cd $ROOTDIR/benchmarks/figure7/workflow2
$ROOTDIR/scripts/generation/generate_lift_kernels_for_handwritten_benchmarks.sh

cd $ROOTDIR/benchmarks/figure8/workflow2
$ROOTDIR/scripts/generation/generate_lift_kernels_for_ppcg_benchmarks.sh
