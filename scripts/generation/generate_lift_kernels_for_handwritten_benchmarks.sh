#!/bin/bash
### README #########################################################################################################
# This script needs to be executed in $ROOTDIR/benchmarks/figure7/workflow2
# It works on the following folder structure:
# benchmarks/architecture/lift
#
# Requirements:
#   HighLevelRewrite, MemoryMappingRewrite and GenericKernelPrinter in PATH
####################################################################################################################
BENCHMARKS=$(ls)
for bench in $BENCHMARKS ; do
        echo -e "\e[34m[INFO] Processing $bench\e[0m"
        pushd $bench > /dev/null

        ARCHITECTURE=$(ls -d */)

        for architecture in $ARCHITECTURE ; do
                pushd $architecture > /dev/null
                ARCH=$(echo $architecture | sed 's/\///')
								pushd lift >/dev/null

                # generate kernels here:
                echo -e "\e[34m[INFO] Generating kernels for $ARCH\e[0m"
                CONFIG=$(ls | grep json)
                HighLevelRewrite --file $CONFIG ../../$bench.lift 
                MemoryMappingRewrite --file $CONFIG $bench 
                GenericKernelPrinter --file $CONFIG $bench 
                echo [INFO] Done generating kernels

                popd > /dev/null
								popd > /dev/null
        done
        popd > /dev/null
done
