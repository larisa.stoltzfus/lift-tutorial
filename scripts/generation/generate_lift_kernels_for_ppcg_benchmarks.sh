#!/bin/bash
### README #########################################################################################################
# This script needs to be executed in $ROOTDIR/benchmarks/figure8/workflow2
# It works on the following folder structure:
# benchmarks/input_size/architecture
#
# It generates lift kernels using the bench.lift file in 
# folder 'input_size' using the config found in 'architecture'
#
# Requirements:
#   HighLevelRewrite, MemoryMappingRewrite and GenericKernelPrinter in PATH
####################################################################################################################

BENCHMARKS=$(ls)
for bench in $BENCHMARKS ; do
        echo -e "\e[34m[INFO] Processing $bench\e[0m"
        pushd $bench > /dev/null
        SIZE=$(ls)

        for size in $SIZE ; do
                echo -e "\e[34m[INFO] Using input size: $size\e[0m"
                pushd $size > /dev/null
                ARCHITECTURE=$(ls -d */)

                for architecture in $ARCHITECTURE ; do
                        pushd $architecture > /dev/null
                        ARCH=$(echo $architecture | sed 's/\///')

                        # generate kernels here:
                        echo -e "\e[34m[INFO] Generating kernels for $ARCH\e[0m"
                        CONFIG=$(ls | grep json)
                        HighLevelRewrite --file $CONFIG ../$bench.lift 
                        MemoryMappingRewrite --file $CONFIG $bench 
                        GenericKernelPrinter --file $CONFIG $bench 
                        echo [INFO] Done generating kernels

                        popd > /dev/null
                done
                popd > /dev/null
        done
        popd > /dev/null
done
