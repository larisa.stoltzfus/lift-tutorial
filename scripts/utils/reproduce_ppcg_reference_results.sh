#!/bin/bash
if [ $# -ne 1 ]
then
    echo "No arguments supplied - which architecture? (kepler | tahiti | mali)?"
    exit -1
fi
if [ "$1" == "kepler" ] || [ "$1" == "tahiti" ] || [ "$1" == "mali" ]
then 
    FOLDERS=$(find . -name "*.json" | grep $1)
    for f in $FOLDERS
    do
            #pushd $f > /dev/null
            #run_best_ppcg_kernel.sh
            echo $f
            #popd > /dev/null
    done
else
    echo "wrong architecture! valid: (kepler | tahiti | mali)?"
    exit -1
fi


