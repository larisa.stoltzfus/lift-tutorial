#!/bin/bash

if [ "$#" -lt 1 ]; then
    echo "Value required as parameter!"
    exit -1
fi

VALUE=$1

# update lift kernel-generation-configs
DIR="${ROOTDIR}/benchmarks/figure7/workflow2"
FILES=$( find $DIR -type f | grep json$ )
PARAM_TO_CHANGE="timeout_in_seconds"
for FILE in ${FILES} ; do
    sed -i "s/.*${PARAM_TO_CHANGE}.*/    \"${PARAM_TO_CHANGE}\" : ${VALUE},/" $FILE
done

DIR="${ROOTDIR}/benchmarks/figure8/workflow2"
FILES=$( find $DIR -type f | grep json$ )
PARAM_TO_CHANGE="timeout_in_seconds"
for FILE in ${FILES} ; do
    sed -i "s/.*${PARAM_TO_CHANGE}.*/    \"${PARAM_TO_CHANGE}\" : ${VALUE},/" $FILE
done

# update ppcg tuning directives
FILES=$( find $DIR -name tuner.sh )
PARAM_TO_CHANGE="chrono::seconds>"

for FILE in ${FILES} ; do
				sed -i "s/$PARAM_TO_CHANGE.*/$PARAM_TO_CHANGE($VALUE)\"/" $FILE
done
